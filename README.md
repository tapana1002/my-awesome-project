
# Data Vision Application

To build the project, run

    $ mkdir build
    $ cd build
    $ cmake ..
    $ make

By default, the project will be compiled to both a library and an executable (via main.cpp).
You can add additional source files in `src/CMakeLists.txt` by modifying the line

    set(SRC ...)

To see other makefile commands run

    $ make help

### Doxygen

Documentation can be generated with

    $ make docs

A tutorial on the doxygen syntax can be found [here](http://www.stack.nl/~dimitri/doxygen/manual/docblocks.html).

### googletest

You can also make use of the googletest library to write and execute automated tests.
Simply create new tests in the `test` directory.
They will be automatically compiled into an executable and can be run with

    make check

For an introduction to writing tests with googletest, follow [this link](http://code.google.com/p/googletest/wiki/Primer).


### coros command line 

To run the coros application, go to the coros binary/excutable path and run the binary as follow:
$ ./CoRos -rgbir -csv=50 -rgb_frame=30 -oni=60 -depth_frame=40

"-rgbir" : run camera in both rgb mode and ir mode
"-rgb" : run camera in rgb mode
"-ir" : run camera in ir mode
"-rgb_frame=50" : enable rgb photo capture function and only capture 50 photos
"-depth_frame=40": enable depth photo capture function and only capture 40 photos
"-oni=60": enable oni/video capture function and only capture 60 seconds of the video