/*
* Copyright Daimler, 2017
* All Rights Reserved
* UNPUBLISHED, LICENSED SOFTWARE.
* CONFIDENTIAL AND PROPRIETARY INFORMATION
* WHICH IS THE PROPERTY OF Daimler.
*
* @file   OrbbecManager.cpp
* @date   June, 2017
* @brief  Orbbec managing module
*
* This file defines the functions that the orbbec manager thread uses to create/modify the orbbec devices.
* Each device has a thread and they are all managed by the orbbec manager thread.
*/

#include "OrbbecManager.h"
#include <libuvc/libuvc.h>

using namespace std;

extern COMMAND_LINE_ARGS g_command_line_args;

/**
 * @name    OrbbecManager
 * @brief   OrbbecManager constructor.
 * @ingroup Orbbec Manager
 * Example Usage:
 * @code
 *    OrbbecManager *orb_manager = new OrbbecManager();
 * @endcode
 */
OrbbecManager::OrbbecManager()
{
	
}

/**
 * @name    ~OrbbecManager
 * @brief   OrbbecManager deconstructor.
 * @ingroup Orbbec Manager
 */
OrbbecManager::~OrbbecManager()
{
	if (m_openni_streams != NULL)
	{
		delete []m_openni_streams;
	}
}

/**
 * @name    orbbec_manager_loop
 * @brief   OrbbecManager thread loop function.
 * @ingroup Orbbec Manager
 * @retval  NONE
 */
void OrbbecManager::orbbec_manager_loop()
{
	bool status = false;
	
	status = init_orbbec_threads();
	if(!status)
	{
		LOG(ERROR) << "Failed to create Orbbec camera threads!";
		exit(EXIT_FAILURE);
	}

	status = start_orbbec_streams();
	if(!status)
	{
		LOG(ERROR) << "Failed to create Orbbec streams!";
		exit(EXIT_FAILURE);
	}

	while(1)
	{
		status = capture_orbbec_streams();
		if(!status)
		{
			LOG(ERROR) << "Failed to read orbbec streams!";
			exit(EXIT_FAILURE);
		}
		// for(int i = 0; i < m_device_num; i++)
		// {	
		// std::cout << "FPS(cam" << i << "): " << m_openni_streams[i]->getVideoMode().getFps();
		// }
		// std::cout << " ---\r"; 
		// std::cout.flush();
	}
}

/**
 * @name    get_orbbec_camera_count
 * @brief   Returns the number of Orbbec cameras
 * @ingroup Orbbec Manager
 * @retval  INT Number of detected orbbec cameras
 */
int  get_orbbec_camera_count()
{
	openni::Status ret_status = openni::STATUS_OK;
	openni::Array<openni::DeviceInfo> tmp_deviceList;
	/** 
	*	Initializes all available sensor drivers and scans the system for available devices.
	*/
	ret_status = openni::OpenNI::initialize();
	if (ret_status != openni::STATUS_OK)
	{
		LOG(ERROR) << "OpenNI Initialize failed " << openni::OpenNI::getExtendedError() << endl;
		return 0;
	}
	
	LOG(INFO) << "Looking for Orbbec Cameras...";
	
	/**
	*	2. Get a list of all available devices connected to the system.
	*/
	openni::OpenNI::enumerateDevices(&tmp_deviceList);


	openni::OpenNI::shutdown();
	/**
	*	3. Get device number of all the available devices.
	*/
	return tmp_deviceList.getSize();
}

/**
 * \name    init_orbbec_threads
 * \brief   Initializes and spawns the orbbec camera threads
 * \ingroup Orbbec Manager
 *
 * This method with first initialize the OpenNI API and lower level drivers,
 * then get a list of all available orbbec devices. It will determine the number
 * of depth and ir streams required and if any devices are Astra Pros to correctly 
 * set the number of color streams. Finally construct Orbbec camera objects and 
 * spawn the correct number of threads.
 *
 * \retval  INT Number of detected orbbec cameras
 */
bool OrbbecManager::init_orbbec_threads()
{
	openni::Status ret_status = openni::STATUS_OK;
	
	//for astra pros
	uvc_error_t uvc_ret_status ;
	
	char dev_uri[30];
	char* uri_substring; 
	
	// initialize the openNI driver
	ret_status = openni::OpenNI::initialize();
	if (ret_status != openni::STATUS_OK)
	{
		LOG(ERROR) << "Initialize failed " << openni::OpenNI::getExtendedError() << endl;
		return false;
	}
	
	//LOG(INFO) << "OpenNI Library Initialized.";
	
	//function for querying all available devices
	openni::OpenNI::enumerateDevices(&m_deviceList);
	
	//LOG(INFO) << "enumerateDevices() done.";

	// Set the device count 
	m_device_num = m_deviceList.getSize();
	
	if(m_device_num < 1)
	{
		LOG(ERROR) << "No Orbbec camera found, please check camera connection.";
		return false;
	}

	uvc_ret_status = uvc_get_device_list(m_ctx, &m_uvc_astra_list);
	if(uvc_ret_status < 0)
	{
		//LOG(TRACE) << "No Astra Pros Found, because uvc list is 0";
	}	

	if(!g_command_line_args.ir_enabled && !g_command_line_args.rgb_save_avi_enabled)
	{
		m_active_depth_stream_index = -1; //for laser control
		m_depth_stream_count = m_device_num;
	}

	if(g_command_line_args.ir_enabled)
	{
		m_ir_stream_count = m_device_num;
	}

	if(g_command_line_args.rgb_enabled)
	{
		for(int i=0; i<m_device_num; i++)
		{
			if(m_deviceList[i].getUsbProductId() != 1027)
			{
				//Not an astra pro, so we'll use openNI
				m_color_stream_count++;
			} else
			{
				//we have an Astra Pro
				int dev_id = 0;
				int dev_usb_num;
				 
				//Extract usb dev # from uri to connect to uvc dev
				strncpy(dev_uri, m_deviceList[i].getUri() ,29);
				uri_substring = strtok(dev_uri,"@");
				uri_substring = strtok(NULL,"@");
				uri_substring = strtok(uri_substring,"/");
				uri_substring = strtok(NULL,"/");
				dev_usb_num = atoi(uri_substring); 

				//traverse uvc dev list to match the openni device to the uvc device by using the usb dev #
				while(m_uvc_astra_list[dev_id] != NULL )
				{
					uvc_device_descriptor_t *desc;
					if (uvc_get_device_descriptor(m_uvc_astra_list[dev_id], &desc) != UVC_SUCCESS)
				    {
				    	dev_id++;
				    	continue;
				    }  
					if(desc->idVendor == m_deviceList[i].getUsbVendorId()){
						int dev_addr = uvc_get_device_address(m_uvc_astra_list[dev_id]);
						dev_addr++; //in libuvc index starts at 0
						if( dev_addr == dev_usb_num)
						{
							m_camera_pairs[i].uvc_orbbec_device = m_uvc_astra_list[dev_id];
						}
					}
					dev_id++;
				}
			}
		}
	}

	LOG(INFO) << "There are " << std::to_string(m_device_num) << " connected Orbbec devices.\n" << std::endl;

	//Determine the total number of OpenNI streams from the Orbbec cameras
	if(g_command_line_args.rgb_enabled && !g_command_line_args.ir_enabled && !g_command_line_args.rgb_save_avi_enabled)
	{
		m_openni_stream_count = m_depth_stream_count + m_color_stream_count;
	} else if(g_command_line_args.ir_enabled && g_command_line_args.rgb_enabled)
	{
		m_openni_stream_count = m_ir_stream_count + m_color_stream_count;
	} else if(!g_command_line_args.ir_enabled && !g_command_line_args.rgb_enabled)
	{
		m_openni_stream_count = m_depth_stream_count;
	} else if(g_command_line_args.ir_enabled && !g_command_line_args.rgb_enabled)
	{
		m_openni_stream_count = m_ir_stream_count;
	} else if(g_command_line_args.rgb_save_avi_enabled)
	{
		m_openni_stream_count = m_color_stream_count;
	}

	LOG(INFO) <<  "Openni streams found:" << m_openni_stream_count;

	m_openni_streams = new openni::VideoStream*[m_openni_stream_count];

	for(int i=0; i<m_device_num; i++)
	{
		//LOG(TRACE) << "Initializing device thread " << i << ", uri=" << m_deviceList[i].getUri();
		OrbbecCamera *camera = new OrbbecCamera(m_deviceList[i].getUri(), m_frame_mutexs[i], m_depth_frame[i], m_color_frame[i], m_ir_frame[i],
												m_depth_streams[i], m_ir_streams[i], m_color_streams[i]);
		m_camera_pairs[i].camera = camera;
		m_camera_pairs[i].uri = m_deviceList[i].getUri();
		//Turnoff its laser incase its still on from before
		m_camera_pairs[i].camera->switchLaser(m_camera_pairs[i].uri, false); // turn off last laser
		m_camera_pairs[i].camera->set_camera_id(i);

		camera->m_camera_thread = std::thread(&OrbbecCamera::camera_loop, camera);
		camera->m_camera_thread.detach();
		//LOG(TRACE) <<  "Spawned Camera thread for camera: " << i;
	}

	//LOG(INFO) <<  "Init Orbbec Cameras Done.";
	return true;
}

bool OrbbecManager::start_orbbec_streams()
{
	openni::Status ret_status = openni::STATUS_OK;
	 //open orbbec devices
	for(int i = 0; i < m_device_num; i++)
	{
		//LOG(INFO) << "Trying to open: " << m_camera_pairs[i].uri;
		ret_status = m_orbbec_devices[i].open(m_camera_pairs[i].uri);
		if (ret_status != openni::STATUS_OK)
		{
			LOG(ERROR) << "Couldn't open device " << m_camera_pairs[i].uri << " " << openni::OpenNI::getExtendedError();
			openni::OpenNI::shutdown();
			return false;
		}

		m_orbbec_devices[i].getProperty((int)ONI_DEVICE_PROPERTY_SERIAL_NUMBER, &m_camera_pairs[i].serialNum);
		m_camera_pairs[i].camera->m_camera_serial = m_camera_pairs[i].serialNum;
		LOG(INFO) <<  "Orbbec Device opened with Serial number: " << m_camera_pairs[i].serialNum;

		//openuvc device if its a pro
		if(m_orbbec_devices[i].getDeviceInfo().getUsbProductId() == 1027)
		{
			//TODO Deal with the Astra pros
		}
	}
	
	/*
	*	Create streams of depth frames from a specific device, 
	*	it takes two arguments, the referenced device and sensor type.
	*/
	if(!g_command_line_args.ir_enabled && !g_command_line_args.rgb_save_avi_enabled )
	{
		for(int i = 0; i < m_device_num; i++)
		{
			ret_status = m_depth_streams[i].create(m_orbbec_devices[i], openni::SENSOR_DEPTH);
			if (ret_status == openni::STATUS_OK)
			{
				//LOG(INFO) << "created depth stream for camera: " << i;
			}
			else 
			{
				LOG(ERROR) << "Couldn't create stream " << openni::SENSOR_DEPTH << "on device "  <<
							 m_camera_pairs[i].uri << endl << openni::OpenNI::getExtendedError();
			}
		}
	}

	/*
	*	Create streams of color frames from a specific device, 
	*	it takes two arguments, the referenced device and sensor type.
	*/
	if(g_command_line_args.rgb_enabled)
	{
		for(int i = 0; i < m_device_num; i++)
		{
			if(m_orbbec_devices[i].getDeviceInfo().getUsbProductId() != 1027)
			{
				ret_status = m_color_streams[i].create(m_orbbec_devices[i], openni::SENSOR_COLOR);
				if (ret_status == openni::STATUS_OK)
				{
					//LOG(INFO) << "created color stream for camera: " << i;
				}
				else 
				{
					LOG(ERROR) << "Couldn't create stream " << openni::SENSOR_COLOR << "on device "  <<
								 m_camera_pairs[i].uri << endl << openni::OpenNI::getExtendedError();
				}
			}
		}
	}

	/*
	*	Create streams of ir frames from a specific device, 
	*	it takes two arguments, the referenced device and sensor type.
	*/
	if(g_command_line_args.ir_enabled)
	{
		for(int i = 0; i < m_device_num; i++)
		{
				ret_status = m_ir_streams[i].create(m_orbbec_devices[i], openni::SENSOR_IR);
				if (ret_status == openni::STATUS_OK)
				{
					//LOG(INFO) << "created ir stream for camera: " << i << "status:" << ret_status;
				}
				else 
				{
					LOG(ERROR) << "Couldn't create stream " << openni::SENSOR_IR << "on device "  <<
								 m_camera_pairs[i].uri << endl << openni::OpenNI::getExtendedError();
				}
		}
	}

	//Start the depth streams and make sure the depth stream is valid.
	if(!g_command_line_args.ir_enabled && !g_command_line_args.rgb_save_avi_enabled)
	{
		for(int i = 0; i < m_device_num; i++)
		{
			ret_status = m_depth_streams[i].start();
			if( ret_status != openni::STATUS_OK )
			{
				LOG(ERROR) << "Couldn't start depth stream:" << openni::OpenNI::getExtendedError();
				m_depth_streams[i].destroy();
				return false;
			} else
			{
				//LOG(INFO) << "started depth stream for camera: " << i;
			}

			if( !m_depth_streams[i].isValid() )
			{
				LOG(ERROR) <<  "No valid streams for device " << m_camera_pairs[i].uri << ". Exiting";
				openni::OpenNI::shutdown();
				return false;
			}
		}
	}

	//Start the color streams and make sure the color stream is valid.
	if(g_command_line_args.rgb_enabled)
	{
		for( int i = 0; i < m_device_num; i++ )
		{	
			if(m_orbbec_devices[i].getDeviceInfo().getUsbProductId() != 1027)
			{
				ret_status = m_color_streams[i].start();
				if( ret_status != openni::STATUS_OK )
				{
					LOG(ERROR) << "Couldn't start color stream:" << openni::OpenNI::getExtendedError();
					m_color_streams[i].destroy();
					return false;
				} else
				{
					//LOG(INFO) << "started color stream for camera: " << i;
				}

				if( !m_color_streams[i].isValid() )
				{
					LOG(ERROR) <<  "No valid streams for device " << m_camera_pairs[i].uri << ". Exiting";
					openni::OpenNI::shutdown();
					return false;
				}
			} else if(m_orbbec_devices[i].getDeviceInfo().getUsbProductId() == 1027)
			{
				//We have an astra Pro, use uvc method for capturing color frames
				//--------------------------------------------------------------------------------------------------
				// uvc_error_t 	  res;
				// //uvc_device_t* 	  uvc_orbbec_device;

				// res = uvc_open(m_camera_pairs[i].uvc_orbbec_device, &m_camera_pairs[i].uvc_dev_handle);
				
				// if(res < 0)
				// {	
				// 	LOG(ERROR) << "Couldn't open uvc device on astra dev id:" << m_camera_pairs[i].serial << "status:" << res;
				// } else
				// {
				// 	LOG(INFO) << "Opened uvc device for camera: " << i;
				// }

				// uvc_print_diag(m_camera_pairs[i].uvc_dev_handle, stdout);

			    //       res = uvc_get_stream_ctrl_format_size(
			    //           m_camera_pairs[i].uvc_dev_handle, &m_camera_pairs[i].ctrl, /* result stored in ctrl */
			    //           UVC_FRAME_FORMAT_MJPEG, /* YUV 422, aka YUV 4:2:2. try _COMPRESSED */
			    //           1280, 720, 30 /* width, height, fps */
			    //       );

			    //       LOG(INFO) << "frame format: " << UVC_FRAME_FORMAT_MJPEG;

			    //       uvc_print_stream_ctrl(&m_camera_pairs[i].ctrl, stdout);

			    //       if (res < 0) {
				//        LOG(ERROR) <<  "Can't get ctrl format: " << res;
				//        uvc_perror(res, "get_mode"); /* device doesn't provide a matching stream */
				//    }
				//----------------------------------------------------------------------------------------------------
			}
		}
	}

	//Start the ir streams and make sure the ir stream is valid.
	if(g_command_line_args.ir_enabled)
	{
		for( int i = 0; i < m_device_num; i++ )
		{
				ret_status = m_ir_streams[i].start();
				if( ret_status != openni::STATUS_OK )
				{
					LOG(ERROR) << "Couldn't start ir stream:" << openni::OpenNI::getExtendedError();
					m_ir_streams[i].destroy();
					return false;
				} else
				{
					//LOG(INFO) << "started ir stream for camera: " << i << "status:" << ret_status;
				}

				if( !m_ir_streams[i].isValid() )
				{
					LOG(ERROR) <<  "No valid streams for device " << m_camera_pairs[i].uri << ". Exiting";
					openni::OpenNI::shutdown();
					return false;
				}
		}
	}

	// if color to depth registration is needed in the point cloud output
	if(g_command_line_args.csv_pointcloudcolor_record_enabled && (g_command_line_args.rgb_enabled)) 
	{
		for( int i = 0; i < m_device_num; i++ )
		{
			
				m_orbbec_devices[i].setImageRegistrationMode(openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR);
		}
	}

	//Initialize the depth frame streams for waitForAnyStream()
	if(!g_command_line_args.ir_enabled && !g_command_line_args.rgb_save_avi_enabled)
	{
		for( int i = 0; i < m_depth_stream_count; i++ )
		{
			m_openni_streams[i] = &m_depth_streams[i];
		}
	}

	//Initialize the ir frame streams for waitForAnyStream()
	if(g_command_line_args.ir_enabled)
	{
		for( int i = 0; i < m_ir_stream_count; i++ )
		{
			m_openni_streams[i] = &m_ir_streams[i];
		}
	}

	if( g_command_line_args.rgb_save_avi_enabled )
	{
		for( int i = 0; i < m_color_stream_count; i++ )
		{
			m_openni_streams[i] = &m_color_streams[i];
		}	
	}
	//Save the color streams with depth for waitForAnyStream()
	if(!g_command_line_args.ir_enabled && g_command_line_args.rgb_enabled &&
		!g_command_line_args.rgb_save_avi_enabled)
	{
		int j = 0;
		for(int i = m_depth_stream_count; i < ( m_depth_stream_count + m_color_stream_count ); i++)
		{
			m_openni_streams[i] = &m_color_streams[j];
			j++;
		}
	}

	//Save the color streams with ir for waitForAnyStream()
	if(g_command_line_args.ir_enabled && g_command_line_args.rgb_enabled)
	{
		int k = 0;
		for(int i = m_ir_stream_count; i < ( m_ir_stream_count + m_color_stream_count ); i++)
		{
			m_openni_streams[i] = &m_color_streams[k];
			k++;
		}
	}	
	//LOG(TRACE) <<"Finished starting all camera streams";

	return true;
}

bool OrbbecManager::capture_orbbec_streams()
{
	int streamIndex;
	openni::Status rc;
	if(g_command_line_args.ir_enabled && g_command_line_args.rgb_enabled)
	{
		rc = openni::OpenNI::waitForAnyStream(m_openni_streams, (m_ir_stream_count + m_color_stream_count), &streamIndex);
	} else if (!g_command_line_args.ir_enabled && g_command_line_args.rgb_enabled && !g_command_line_args.rgb_save_avi_enabled)
	{
		rc = openni::OpenNI::waitForAnyStream(m_openni_streams, (m_depth_stream_count + m_color_stream_count), &streamIndex);
	}else
	{
		rc = openni::OpenNI::waitForAnyStream(m_openni_streams, (m_openni_stream_count), &streamIndex);
	}
	if (rc != openni::STATUS_OK)
	{
		LOG(ERROR) <<"Error trying to get frames";
		return false;
	}

	if( (streamIndex < m_device_num) && (streamIndex >= 0) )
	{
		if (!g_command_line_args.ir_enabled && !g_command_line_args.rgb_save_avi_enabled)
		{
			/* We must enable only the active depth camera's ir projection laser*/
			if(m_active_depth_stream_index < 0 )
			{
				m_active_depth_stream_index = streamIndex;
			} else if(m_active_depth_stream_index != streamIndex){
				m_camera_pairs[m_active_depth_stream_index].camera->switchLaser(m_camera_pairs[m_active_depth_stream_index].uri, false); //turn off previous
				m_active_depth_stream_index = streamIndex;
				m_camera_pairs[m_active_depth_stream_index].camera->switchLaser(m_camera_pairs[m_active_depth_stream_index].uri, true); //turn off previous
			}

			rc = m_depth_streams[streamIndex].readFrame(&m_depth_frame[streamIndex]);
			m_frame_mutexs[streamIndex].lock();
			m_camera_pairs[streamIndex].camera->set_current_depth_frame(m_depth_frame[streamIndex]);
			m_frame_mutexs[streamIndex].unlock(); 
			//LOG(TRACE) <<"Got Depth Frame From stream "<< streamIndex << ",status:" << rc;
		} else if(g_command_line_args.rgb_save_avi_enabled) {
			if(g_command_line_args.rgb_enabled)
			{
				rc = m_color_streams[streamIndex].readFrame(&m_color_frame[streamIndex]);
				m_frame_mutexs[streamIndex].lock();
				m_camera_pairs[streamIndex].camera->set_current_color_frame(m_color_frame[streamIndex]);
				m_frame_mutexs[streamIndex].unlock();
				//LOG(TRACE) <<"Got Color Frame From stream "<< streamIndex << ",status:" << rc;
			}
		}
		else
		{
			m_camera_pairs[m_active_depth_stream_index].camera->switchLaser(m_camera_pairs[m_active_depth_stream_index].uri, false);
			rc = m_ir_streams[streamIndex].readFrame(&m_ir_frame[streamIndex]);
			m_frame_mutexs[streamIndex].lock();
			m_camera_pairs[streamIndex].camera->set_current_ir_frame(m_ir_frame[streamIndex]);
			m_frame_mutexs[streamIndex].unlock(); 
			//LOG(TRACE) <<"Got IR Frame From stream "<< streamIndex << ",status:" << rc;
		}
	} else if(streamIndex >= m_device_num)
	{
		int dev_index = streamIndex - m_device_num;
		if(g_command_line_args.rgb_enabled)
		{
			rc = m_color_streams[dev_index].readFrame(&m_color_frame[dev_index]);
			m_frame_mutexs[dev_index].lock();
			m_camera_pairs[dev_index].camera->set_current_color_frame(m_color_frame[dev_index]);
			m_frame_mutexs[dev_index].unlock();
			//LOG(TRACE) <<"Got Color Frame From stream "<< streamIndex << ",status:" << rc;
		}
	}

	return true;
}
