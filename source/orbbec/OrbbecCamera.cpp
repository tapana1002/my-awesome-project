
/* ========================================
*
* Copyright Daimler, 2017
* All Rights Reserved
* UNPUBLISHED, LICENSED SOFTWARE.
*
* CONFIDENTIAL AND PROPRIETARY INFORMATION
* WHICH IS THE PROPERTY OF Daimler.
*
* @file   OrbbecCamera.cpp
* @date   June, 2017
* @brief  Orbbec camera module
*
* ========================================
*/
#include "CoROS.h"
#include "OrbbecCamera.h"
#include <GL/glut.h>
#include <jsoncpp/json/json.h>
#include <jsoncpp/json/writer.h>
#include "opencv2/opencv.hpp"
#include <zbar.h>

using namespace std;
using namespace cv;

extern COMMAND_LINE_ARGS g_command_line_args;

/**
*	These two variables are passed in from command line and used by glut
*/
extern int g_argc; 
extern char **g_argv;

std::mutex  g_device_mtx;
std::mutex  g_frame_mtx;

/*
 * @name    OrbbecCamera
 * @brief   OrbbecCamera constructor.
 * @Descr Instantiate camera object(s) and start camera_loop() thread as per camera
 *		  Initialize the private member variables
 * @param1 uri: the device unique uri 
 * @param2 
 */
OrbbecCamera::OrbbecCamera(const char* uri, std::mutex& m_frame_mtx,
	openni::VideoFrameRef& dframe, openni::VideoFrameRef& cframe, openni::VideoFrameRef& iframe,
	openni::VideoStream& depth_ref, openni::VideoStream& ir_ref, openni::VideoStream& rgb_ref):
	m_camera_id(-1),m_camera_uri(uri),m_thread_id(-1), m_frame_mtx_ref(m_frame_mtx),
	m_depth_frame_ref(dframe), m_color_frame_ref(cframe), m_ir_frame_ref(iframe),
	m_depth_stream_ref(depth_ref),m_ir_stream_ref(ir_ref),m_rgb_stream_ref(rgb_ref)
{
	m_zbar_enabled = false;
	/* 
	TODO: add usb related code here
		add any other definitions that would not be changed later
	*/
}

OrbbecCamera::~OrbbecCamera()
{
	//deconstructor
}

/*
*	\descr camera loop thread function
*	1. initialize the OpenGL through initOpenGL
*	2. start read frame and display frame loop 
*	\param 
*/
void OrbbecCamera::camera_loop()
{

	// //start_camera_opengl_thread();

	while(1)
	{
		process_frame();
	}
}

/*
 * \Descr record openni frame in to different format: csv, jpeg, json, oni
 * \param
 */
void OrbbecCamera::record_frame()
{
	if(g_command_line_args.csv_depth_record_enabled)
	{	
		save_depth_frame_csv();
	} else if(g_command_line_args.csv_pointcloud_record_enabled)
	{
		save_point_cloud_csv();
	} else if(g_command_line_args.csv_pointcloudcolor_record_enabled)
	{
	 	save_point_cloud_color_csv();
	} else if(g_command_line_args.json_depth_record_enabled)
	{
		save_depth_frame_json();
	} else if(g_command_line_args.json_pointcloud_record_enabled)
	{
		save_point_cloud_json();
	} else if(g_command_line_args.json_pointcloudcolor_record_enabled)
	{
		save_point_cloud_color_json();
	}
	else if(g_command_line_args.ply_pointcloud_record_enabled)
	{
		save_point_cloud_ply();
	} else if(g_command_line_args.ply_pointcloudcolor_record_enabled)
	{
		save_point_cloud_color_ply();
	} else if(g_command_line_args.rgb_save_jpeg_enabled)
	{
	 	save_rgb_image();
	} else if(g_command_line_args.depth_save_jpeg_enabled)
	{
	 	save_depth_image();
	}else if(g_command_line_args.ir_save_jpeg_enabled)
	{
	 	save_ir_image();
	}
	else if(g_command_line_args.rgb_save_avi_enabled)
	{
	 	save_rgb_video();
	}
	else if(g_command_line_args.zbar_enabled && !m_zbar_enabled)
	{
	 	zbar_scan_color();
	}
}

void OrbbecCamera::switchLaser(const char* uri, bool turnOn)
{
	COBDevice  device;
	device.InitDevice();
	device.OpenDevice(uri);

	if (turnOn)
	{
		*(uint16_t*)m_driver_cmdbuf1 = 1;
	}
	else
	{
		*(uint16_t*)m_driver_cmdbuf1 = 0;
	}
	
	device.SendCmd(85, m_driver_cmdbuf1, 2, m_driver_cmdbuf2, 2);
	device.CloseDevice();
}

/*
*	\descr  process frames 
*	\param
**/
void OrbbecCamera::process_frame()
{
	record_frame();
	//TODO:displayframe()
	//TODO:have zbar process frame
}

/*
*	\descr camera loop thread function
*	1. initialize the OpenGL through initOpenGL
*	2. start read frame and display frame loop 
*	\param 
*/
void OrbbecCamera::opengl_glut_loop()
{
 //    openni::Status ret_status = openni::STATUS_OK;

	// m_glut_thread_id =  pthread_self();
	// cout << "openglthread_id=" << m_glut_thread_id << endl;

	// Viewer *camera_viewer = new Viewer("Coros Viewer", m_device, m_depth_stream, m_color_stream, m_frame_mtx, m_depth_ref_frame, m_color_ref_frame);
	// ret_status = camera_viewer->init(g_command_line_args.argc, g_command_line_args.argv);
	// if (ret_status != openni::STATUS_OK)
	// {
	// 	openni::OpenNI::shutdown();
	// }
	
 //    camera_viewer->run();

    // place holder for compiler not to complain
    while(1){};
}

void OrbbecCamera::get_current_depth_frame(openni::VideoFrameRef& frame_copy)
{
	frame_copy = m_depth_frame_ref;
}


bool OrbbecCamera::set_current_depth_frame(openni::VideoFrameRef frame)
{
	m_depth_frame_ref = frame;
	return true;
}

void OrbbecCamera::get_current_color_frame(openni::VideoFrameRef& frame_copy)
{
	frame_copy = m_color_frame_ref;
}


bool OrbbecCamera::set_current_color_frame(openni::VideoFrameRef frame)
{
	m_color_frame_ref = frame;
	return true;
}

void OrbbecCamera::get_current_ir_frame(openni::VideoFrameRef& frame_copy)
{
	frame_copy = m_ir_frame_ref;
}


bool OrbbecCamera::set_current_ir_frame(openni::VideoFrameRef frame)
{
	m_ir_frame_ref = frame;
	return true;
}

/*
*	\descr record frames into a avi file
*	\param 
*/
void OrbbecCamera::save_frames_2_video()
{

}

void OrbbecCamera::save_depth_frame_csv()
{
	openni::VideoFrameRef current_depth_frame;
	static uint64_t 	  frame_counter = 0;

	//grab current frame
	m_frame_mtx_ref.lock();
	get_current_depth_frame(current_depth_frame);
	m_frame_mtx_ref.unlock();

	if(current_depth_frame.isValid())
	{
		const openni::DepthPixel* pDepthArray = reinterpret_cast<const openni::DepthPixel*>( current_depth_frame.getData() );

		int imageWidth = current_depth_frame.getWidth(),
			imageHeight = current_depth_frame.getHeight();
		int curr_frame_timestamp = (current_depth_frame.getTimestamp()/1000);

		string csv_file_name;
		csv_file_name = "orb-camera" + to_string(m_camera_id) + "-tmp.csv";
		ofstream csv_file(csv_file_name);

		for( int y = 0; y < imageHeight; ++ y )
		{
			for( int x = 0; x < imageWidth; ++ x )
			{
				int idx = x + y * imageWidth;
				// update points array
				const openni::DepthPixel& rDepth = pDepthArray[idx];
				
				if( rDepth != 0 )
				{
					csv_file << x << "," << y << "," << rDepth << "\n";

				}

					
			}
		}
	    string timestamp_str = "orb-" + to_string(m_camera_id) + "-depth-" + to_string(frame_counter) + 
#if TIME_STAMP_NEEDED
                               "-" + to_string(curr_frame_timestamp) + "ms" + 
#endif
                               ".csv";
		frame_counter++;
		rename(csv_file_name.c_str(),  timestamp_str.c_str()); 
	}
}

/*
*	\descr extract frames into a .csv file in the form of R,G,B,D,R,G,B,D...
*	\param 
*/
void OrbbecCamera::save_point_cloud_csv()
{
	openni::VideoFrameRef current_depth_frame;
	static uint64_t 	  frame_counter = 0;

	//grab current frame
	m_frame_mtx_ref.lock();
	get_current_depth_frame(current_depth_frame);
	m_frame_mtx_ref.unlock();


	if( current_depth_frame.isValid() )
	{
		const openni::DepthPixel* pDepthArray = reinterpret_cast<const openni::DepthPixel*>( current_depth_frame.getData() );

		int imageWidth = current_depth_frame.getWidth(),
			imageHeight = current_depth_frame.getHeight();
		int curr_frame_timestamp = (current_depth_frame.getTimestamp()/1000);	

		string csv_file_name;
		csv_file_name = "orb-camera" + to_string(m_camera_id) + "-tmp.csv";
		ofstream csv_file(csv_file_name);

		float vector_world_pos[3]; // the vector
		for( int y = 0; y < imageHeight; ++ y )
		{
			for( int x = 0; x < imageWidth; ++ x )
			{
				int idx = x + y * imageWidth;
				// update points array
				const openni::DepthPixel& rDepth = pDepthArray[idx];
				
				if( rDepth != 0 )
				{
					openni::CoordinateConverter::convertDepthToWorld( m_depth_stream_ref, x, y, rDepth,
	 														 &vector_world_pos[0], &vector_world_pos[1], &vector_world_pos[2] );
					csv_file << to_string(vector_world_pos[0]) << "," << to_string(vector_world_pos[1]) << "," << to_string(vector_world_pos[2]) << "\n";
				}

					
			}
		}
	    string timestamp_str = "orb-" + to_string(m_camera_id) + "-point-cloud-" + to_string(frame_counter) + 
#if TIME_STAMP_NEEDED
                               "-" + to_string(curr_frame_timestamp) + "ms" + 
#endif
                               ".csv";
		frame_counter++;
		rename(csv_file_name.c_str(),  timestamp_str.c_str());
	}
}

/*
*	\descr extract frames into a .csv file in the form of X,Y,Z,R,G,B\n
*	\param 
*/
void OrbbecCamera::save_point_cloud_color_csv()
{
	openni::VideoFrameRef current_depth_frame;
	openni::VideoFrameRef current_color_frame;
	static uint64_t 	  frame_counter = 0;

	//grab current frame
	m_frame_mtx_ref.lock();
	get_current_depth_frame(current_depth_frame);
	get_current_color_frame(current_color_frame);
	m_frame_mtx_ref.unlock();


	if( current_depth_frame.isValid() && current_color_frame.isValid() )
	{
		const openni::DepthPixel* pDepthArray = reinterpret_cast<const openni::DepthPixel*>( current_depth_frame.getData() );
		const openni::RGB888Pixel* pColorArray = reinterpret_cast<const openni::RGB888Pixel*>( current_color_frame.getData() );

		int imageWidth = current_depth_frame.getWidth(),
			imageHeight = current_depth_frame.getHeight();
		int curr_frame_timestamp = (current_depth_frame.getTimestamp()/1000);	

		string csv_file_name;
		csv_file_name = "orb-camera" + to_string(m_camera_id) + "-tmp.csv";
		ofstream csv_file(csv_file_name);

		float vector_world_pos[3]; // the vector
		for( int y = 0; y < imageHeight; ++ y )
		{
			for( int x = 0; x < imageWidth; ++ x )
			{
				int idx = x + y * imageWidth;
				// update points array
				const openni::DepthPixel& rDepth = pDepthArray[idx];
				
				if( rDepth != 0 )
				{
					const openni::RGB888Pixel& rColor = pColorArray[idx];
					openni::CoordinateConverter::convertDepthToWorld( m_depth_stream_ref, x, y, rDepth,
	 														 &vector_world_pos[0], &vector_world_pos[1], &vector_world_pos[2] );
					csv_file << to_string(vector_world_pos[0]) << "," << to_string(vector_world_pos[1]) << "," << to_string(vector_world_pos[2]) << ",";
	 				csv_file << to_string(rColor.r) << "," << to_string(rColor.g) << "," << to_string(rColor.b) << endl;
				}

					
			}
		}
	    string timestamp_str = "orb-" + to_string(m_camera_id) + "-point-cloud-color-" + to_string(frame_counter) + 
#if TIME_STAMP_NEEDED
                               "-" + to_string(curr_frame_timestamp) + "ms" + 
#endif
                               ".csv";
		frame_counter++;
		rename(csv_file_name.c_str(),  timestamp_str.c_str());
	}
}


/*
*	\descr extract frames into video
*	\param 
*/
void OrbbecCamera::save_depth_frame_json()
{
	openni::VideoFrameRef current_depth_frame;
	static uint64_t 	  frame_counter = 0;

	//grab current frame
	m_frame_mtx_ref.lock();
	get_current_depth_frame(current_depth_frame);
	m_frame_mtx_ref.unlock();

	if(current_depth_frame.isValid())
	{
		const openni::DepthPixel* pDepthArray = reinterpret_cast<const openni::DepthPixel*>( current_depth_frame.getData() );

		string json_file_name;
		json_file_name = "orb-camera" + to_string(m_camera_id) + "-tmp.json";
		ofstream json_file(json_file_name);

		int image_width = current_depth_frame.getWidth();
		int image_height = current_depth_frame.getHeight();
		int curr_frame_timestamp = (current_depth_frame.getTimestamp()/1000);

		Json::Value depth_frame; 
	    depth_frame["timestamp"] = std::to_string(curr_frame_timestamp); 
	    depth_frame["cameraId"] = m_camera_id;
	    depth_frame["imageWidth"] = image_width;
	    depth_frame["imageHeight"] = image_height;

		for(int row=0; row<image_height; row++)
	    {
	    	for(int col=0; col<image_width; col++)
	    	{
	    		int pixel_index = row*image_width + col;
	    		
	    		if(0 == pDepthArray[pixel_index]) continue;

	    		string pixel_name = "p" + to_string(pixel_index);	
			    depth_frame["imagePixelData"][pixel_name]["x"] = col;
			    depth_frame["imagePixelData"][pixel_name]["y"] = row;
			    depth_frame["imagePixelData"][pixel_name]["i"] = (row*640)+col;
			    depth_frame["imagePixelData"][pixel_name]["z"] = pDepthArray[pixel_index];	//depth value from openni
			}
	    }
		json_file << depth_frame << std::endl;
    	json_file.close();

	    string timestamp_str = "orb-" + to_string(m_camera_id) + "-depth-" + to_string(frame_counter) + 
#if TIME_STAMP_NEEDED
                               "-" + to_string(curr_frame_timestamp) + "ms" + 
#endif
                               ".json";
		frame_counter++;
		rename(json_file_name.c_str(),  timestamp_str.c_str()); 
	}
}

void OrbbecCamera::save_point_cloud_json()
{
	openni::VideoFrameRef current_depth_frame;
	static uint64_t 	  frame_counter = 0;

	//grab current frame
	m_frame_mtx_ref.lock();
	get_current_depth_frame(current_depth_frame);
	m_frame_mtx_ref.unlock();

	if(current_depth_frame.isValid())
	{
		const openni::DepthPixel* pDepthArray = reinterpret_cast<const openni::DepthPixel*>( current_depth_frame.getData() );

		string json_file_name;
		json_file_name = "orb-camera" + to_string(m_camera_id) + "-tmp.json";
		ofstream json_file(json_file_name);

		int image_width = current_depth_frame.getWidth(),
		image_height = current_depth_frame.getHeight();
		int curr_frame_timestamp = (current_depth_frame.getTimestamp()/1000);

		Json::Value depth_frame; 
	    depth_frame["timestamp"] = std::to_string(curr_frame_timestamp); 
	    depth_frame["cameraId"] = m_camera_id;
	    depth_frame["imageWidth"] = image_width;
	    depth_frame["imageHeight"] = image_height;

		float vector_world_pos[3]; // the vector
		for(int row=0; row<image_height; row++)
	    {
	    	for(int col=0; col<image_width; col++)
	    	{
	    		int pixel_index = row*image_width + col;
	    		
	    		if(0 == pDepthArray[pixel_index]) continue;
	    		openni::CoordinateConverter::convertDepthToWorld( m_depth_stream_ref, row, col, pDepthArray[pixel_index],
	 														 &vector_world_pos[0], &vector_world_pos[1], &vector_world_pos[2] );

	    		string pixel_name = "p" + to_string(pixel_index);	
			    depth_frame["imagePixelData"][pixel_name]["x"] = vector_world_pos[0];
			    depth_frame["imagePixelData"][pixel_name]["y"] = vector_world_pos[1];
			    depth_frame["imagePixelData"][pixel_name]["z"] = vector_world_pos[2];
			    depth_frame["imagePixelData"][pixel_name]["i"] = (row*640)+col;
			}
	    }
		json_file << depth_frame << std::endl;
    	json_file.close();

	    string timestamp_str = "orb-" + to_string(m_camera_id) + "-point-cloud-" + to_string(frame_counter) + 
#if TIME_STAMP_NEEDED
                               "-" + to_string(curr_frame_timestamp) + "ms" + 
#endif
                               ".json";
		frame_counter++;
		rename(json_file_name.c_str(),  timestamp_str.c_str()); 
	}
}

void OrbbecCamera::save_point_cloud_color_json()
{
	openni::VideoFrameRef current_depth_frame;
	openni::VideoFrameRef current_color_frame;
	static uint64_t 	  frame_counter = 0;

	//grab current frame
	m_frame_mtx_ref.lock();
	get_current_depth_frame(current_depth_frame);
	get_current_color_frame(current_color_frame);
	m_frame_mtx_ref.unlock();
	
	if(current_depth_frame.isValid() && current_color_frame.isValid())
	{
		const openni::DepthPixel* pDepthArray = reinterpret_cast<const openni::DepthPixel*>( current_depth_frame.getData() );
		const openni::RGB888Pixel* pColorArray = reinterpret_cast<const openni::RGB888Pixel*>( current_color_frame.getData() );

		string json_file_name;
		json_file_name = "orb-camera" + to_string(m_camera_id) + "-tmp.json";
		ofstream json_file(json_file_name);

		int image_width = current_depth_frame.getWidth();
		int image_height = current_depth_frame.getHeight();
		int curr_frame_timestamp = (current_depth_frame.getTimestamp()/1000);

		Json::Value depth_frame; 
	    depth_frame["timestamp"] = std::to_string(curr_frame_timestamp); 
	    depth_frame["cameraId"] = m_camera_id;
	    depth_frame["imageWidth"] = image_width;
	    depth_frame["imageHeight"] = image_height;

		float vector_world_pos[3]; // the vector
		for(int row=0; row<image_height; row++)
	    {
	    	for(int col=0; col<image_width; col++)
	    	{
	    		int pixel_index = row*image_width + col;
	    		
	    		if(0 == pDepthArray[pixel_index]) continue;
	    		const openni::RGB888Pixel& rColor = pColorArray[pixel_index];
	    		openni::CoordinateConverter::convertDepthToWorld( m_depth_stream_ref, row, col, pDepthArray[pixel_index],
	 														 &vector_world_pos[0], &vector_world_pos[1], &vector_world_pos[2] );

	    		string pixel_name = "p" + to_string(pixel_index);	
			    depth_frame["imagePixelData"][pixel_name]["x"] = vector_world_pos[0];
			    depth_frame["imagePixelData"][pixel_name]["y"] = vector_world_pos[1];
			    depth_frame["imagePixelData"][pixel_name]["z"] = vector_world_pos[2];
			    depth_frame["imagePixelData"][pixel_name]["r"] = rColor.r;
			    depth_frame["imagePixelData"][pixel_name]["g"] = rColor.g;
			    depth_frame["imagePixelData"][pixel_name]["b"] = rColor.b;	//depth value from openni
			    depth_frame["imagePixelData"][pixel_name]["i"] = (row*640)+col;
			}
	    }
		json_file << depth_frame << std::endl;
    	json_file.close();

	    string timestamp_str = "orb-" + to_string(m_camera_id) + "-point-cloud-color-" + to_string(frame_counter) + 
#if TIME_STAMP_NEEDED
                               "-" + to_string(curr_frame_timestamp) + "ms" + 
#endif
                               ".json";
		frame_counter++;
		rename(json_file_name.c_str(),  timestamp_str.c_str()); 
	}
}

void OrbbecCamera::save_point_cloud_ply()
{
	openni::VideoFrameRef current_depth_frame;
	static uint64_t 	  frame_counter = 0;
	std::string 		  ply_header;
    int 				  point_count = 0;

	//grab current frame
	m_frame_mtx_ref.lock();
	get_current_depth_frame(current_depth_frame);
	m_frame_mtx_ref.unlock();

	if( current_depth_frame.isValid() )
	{
		const openni::DepthPixel* pDepthArray = reinterpret_cast<const openni::DepthPixel*>( current_depth_frame.getData() );

		int imageWidth = current_depth_frame.getWidth(),
			imageHeight = current_depth_frame.getHeight();
		int curr_frame_timestamp = (current_depth_frame.getTimestamp()/1000);	

		string ply_file_name;
		ply_file_name = "orb-camera" + to_string(m_camera_id) + "-tmp.ply";
		ofstream ply_file(ply_file_name);

		float vector_world_pos[3]; // the vector
		std::string point_cloud_points;
		for( int y = 0; y < imageHeight; ++ y )
		{
			for( int x = 0; x < imageWidth; ++ x )
			{
				int idx = x + y * imageWidth;
				point_count++;
				// update points array
				const openni::DepthPixel& rDepth = pDepthArray[idx];
				
				if(rDepth == 0)
	            {
	                point_count--;
	                continue;
	            }
				openni::CoordinateConverter::convertDepthToWorld(m_depth_stream_ref, x, y, rDepth,
 														 &vector_world_pos[0], &vector_world_pos[1], &vector_world_pos[2] );
				std::string row = to_string(vector_world_pos[0]) + " " + to_string(vector_world_pos[1]) + " " + to_string(vector_world_pos[2]) + "\n";
            	point_cloud_points.append(row);	
				
			}
		}
	    string timestamp_str = "orb-" + to_string(m_camera_id) + "-point-cloud-" + to_string(frame_counter) + 
#if TIME_STAMP_NEEDED
                               "-" + to_string(curr_frame_timestamp) + "ms" + 
#endif
                               ".ply";
		frame_counter++;
		ply_header = "ply\nformat ascii 1.0\nelement vertex " + std::to_string(point_count) + "\nproperty float32 x\n"
                + "property float32 y\nproperty float32 z\nelement face 0\nproperty list uint8 int32 vertex_indices\n" + "end_header\n";
    
    	ply_file << ply_header;
    	ply_file << point_cloud_points;
    	ply_file.close();
		rename(ply_file_name.c_str(),  timestamp_str.c_str());
	}
}

void OrbbecCamera::save_point_cloud_color_ply()
{
	openni::VideoFrameRef current_depth_frame;
	openni::VideoFrameRef current_color_frame;
	static uint64_t 	  frame_counter = 0;
	std::string 		  ply_header;
    int 				  point_count = 0;

	//grab current frame
	m_frame_mtx_ref.lock();
	get_current_depth_frame(current_depth_frame);
	get_current_color_frame(current_color_frame);
	m_frame_mtx_ref.unlock();


	if( current_depth_frame.isValid() && current_color_frame.isValid() )
	{
		const openni::DepthPixel* pDepthArray = reinterpret_cast<const openni::DepthPixel*>( current_depth_frame.getData() );
		const openni::RGB888Pixel* pColorArray = reinterpret_cast<const openni::RGB888Pixel*>( current_color_frame.getData() );

		int imageWidth = current_depth_frame.getWidth(),
			imageHeight = current_depth_frame.getHeight();
		int curr_frame_timestamp = (current_depth_frame.getTimestamp()/1000);	

		string ply_file_name;
		ply_file_name = "orb-camera" + to_string(m_camera_id) + "-tmp.ply";
		ofstream ply_file(ply_file_name);

		float vector_world_pos[3]; // the vector
		std::string point_cloud_points;
		for( int y = 0; y < imageHeight; ++ y )
		{
			for( int x = 0; x < imageWidth; ++ x )
			{
				int idx = x + y * imageWidth;
				point_count++;
				// update points array
				const openni::DepthPixel& rDepth = pDepthArray[idx];
				
				if(rDepth == 0)
	            {
	                point_count--;
	                continue;
	            }
	            const openni::RGB888Pixel& rColor = pColorArray[idx];
	            openni::CoordinateConverter::convertDepthToWorld( m_depth_stream_ref, x, y, rDepth,
	 																&vector_world_pos[0], &vector_world_pos[1], &vector_world_pos[2] );
	            std::string row = to_string(vector_world_pos[0]) + " " + to_string(vector_world_pos[1]) + " " + to_string(vector_world_pos[2]) + " " 
	 						 + to_string(rColor.r) + " " + to_string(rColor.g) + " " + to_string(rColor.b) + "\n";
	            point_cloud_points.append(row);	
			}
		}
	    string timestamp_str = "orb-" + to_string(m_camera_id) + "-point-cloud-color-" + to_string(frame_counter) + 
#if TIME_STAMP_NEEDED
                               "-" + to_string(curr_frame_timestamp) + "ms" + 
#endif
                               ".ply";
		frame_counter++;
		ply_header = "ply\nformat ascii 1.0\nelement vertex " + std::to_string(point_count) + "\nproperty float32 x\n"
                + "property float32 y\nproperty float32 z\nproperty uchar r\nproperty uchar g\nproperty uchar b" 
                + "\nelement face 0\nproperty list uint8 int32 vertex_indices\n" + "end_header\n";
    
    	ply_file << ply_header;
    	ply_file << point_cloud_points;
    	ply_file.close();
		rename(ply_file_name.c_str(),  timestamp_str.c_str());

	}
}

void OrbbecCamera::save_depth_image()
{
	openni::VideoFrameRef current_depth_frame;
	cv::Mat depth_image(480, 640, CV_16UC1);
	int step = depth_image.step;

	static int frame_cnts = 0;

    std::vector<int> compression_params; //vector that stores the compression parameters of the image
    compression_params.push_back(CV_IMWRITE_JPEG_QUALITY); //specify the compression technique
    compression_params.push_back(100); //specify the compression quality
    

    m_frame_mtx_ref.lock();
	get_current_depth_frame(current_depth_frame);
	m_frame_mtx_ref.unlock();


    if( current_depth_frame.isValid() )
    {
    	const openni::DepthPixel* pDepthArray = reinterpret_cast<const openni::DepthPixel*>( current_depth_frame.getData() );
        std::string color_image_name = "orb-" + std::to_string(m_camera_id) + "-depth-" + std::to_string(frame_cnts) + 
#if TIME_STAMP_NEEDED
                                        "-" + std::to_string(current_depth_frame.getTimestamp()/1000) + "ms" + 
#endif
                                        ".jpeg";

        memcpy(depth_image.data, pDepthArray, step*480);
        depth_image.convertTo(depth_image,CV_8UC1);
        cv::equalizeHist( depth_image, depth_image );
        cv::applyColorMap(depth_image, depth_image, cv::COLORMAP_WINTER);
        cv::imwrite(color_image_name, depth_image, compression_params);
        frame_cnts++;
    }

}

void OrbbecCamera::save_rgb_image()
{
	openni::VideoFrameRef current_color_frame;

	static int frame_cnts = 0;


    std::vector<int> compression_params; //vector that stores the compression parameters of the image
    compression_params.push_back(CV_IMWRITE_JPEG_QUALITY); //specify the compression technique
    compression_params.push_back(100); //specify the compression quality
    

    m_frame_mtx_ref.lock();
	get_current_color_frame(current_color_frame);
	m_frame_mtx_ref.unlock();

    if( current_color_frame.isValid() )
    {
    	const openni::RGB888Pixel* pColorArray = reinterpret_cast<const openni::RGB888Pixel*>( current_color_frame.getData() );
        std::string color_image_name = "orb-" + std::to_string(m_camera_id) + "-color-" + std::to_string(frame_cnts) + 
#if TIME_STAMP_NEEDED
                                        "-" + std::to_string(current_color_frame.getTimestamp()/1000) + "ms" + 
#endif
                                        ".jpeg";
        cv::Mat color_image(cv::Size(640, 480), CV_8UC3, (void*)pColorArray, cv::Mat::AUTO_STEP); 
        cv::Mat converted_image;
        cv::cvtColor(color_image, converted_image, CV_BGR2RGB);
        cv::imwrite(color_image_name, converted_image, compression_params);
        frame_cnts++;
    }
}

void OrbbecCamera::save_ir_image()
{
	openni::VideoFrameRef current_ir_frame;
	cv::Mat ir_image(480, 640, CV_16UC1);
	int step = ir_image.step;

	static int frame_cnts = 0;

    std::vector<int> compression_params; //vector that stores the compression parameters of the image
    compression_params.push_back(CV_IMWRITE_JPEG_QUALITY); //specify the compression technique
    compression_params.push_back(100); //specify the compression quality
    

    m_frame_mtx_ref.lock();
	get_current_ir_frame(current_ir_frame);
	m_frame_mtx_ref.unlock();


    if( current_ir_frame.isValid() )
    {
    	const openni::Grayscale16Pixel* pIrArray = reinterpret_cast<const openni::Grayscale16Pixel*>( current_ir_frame.getData() );
        std::string color_image_name = "orb-" + std::to_string(m_camera_id) + "-ir-" + std::to_string(frame_cnts) + 
#if TIME_STAMP_NEEDED
                                        "-" + std::to_string(current_ir_frame.getTimestamp()/1000) + "ms" + 
#endif
                                        ".jpeg";

        memcpy(ir_image.data, pIrArray, step*480);
        ir_image.convertTo(ir_image,CV_8UC1);
        cv::equalizeHist( ir_image, ir_image );
        cv::applyColorMap(ir_image, ir_image, cv::COLORMAP_BONE);
        cv::imwrite(color_image_name, ir_image, compression_params);
        frame_cnts++;
    }
}

void OrbbecCamera::save_rgb_video()
{
	static int video_cnts = 0;
	openni::VideoFrameRef current_color_frame;
	static cv::VideoWriter *video_ptr = NULL;

	string video_file_name = "orb-vid-";
	video_file_name.append(std::to_string(video_cnts));
	video_file_name.append(".avi");

	m_frame_mtx_ref.lock();
	get_current_color_frame(current_color_frame);
	m_frame_mtx_ref.unlock();

    if( current_color_frame.isValid() )
    {
    	const openni::RGB888Pixel* pColorArray = reinterpret_cast<const openni::RGB888Pixel*>( current_color_frame.getData() );
    	cv::Mat color_image(cv::Size(640, 480), CV_8UC3, (void*)pColorArray, cv::Mat::AUTO_STEP);  
        cv::Mat converted_image;
        cv::cvtColor(color_image, converted_image, CV_BGR2RGB);

    	if(video_cnts == 0)
    	{	
    		video_ptr = new cv::VideoWriter(video_file_name, CV_FOURCC('M', 'J', 'P', 'G'), 30, cv::Size(converted_image.cols,converted_image.rows), true);
    		video_cnts++;
    	}
    	
    	video_ptr->write(converted_image);
    }
}

void OrbbecCamera::zbar_scan_color()
{
	openni::VideoFrameRef current_color_frame;


    m_frame_mtx_ref.lock();
	get_current_color_frame(current_color_frame);
	m_frame_mtx_ref.unlock();

    if( current_color_frame.isValid() )
    {
    	const openni::RGB888Pixel* pColorArray = reinterpret_cast<const openni::RGB888Pixel*>( current_color_frame.getData() );

        cv::Mat color_image(cv::Size(640, 480), CV_8UC3, (void*)pColorArray, cv::Mat::AUTO_STEP); 
        cv::Mat converted_image;
        cv::cvtColor(color_image, converted_image, CV_BGR2RGB);

        std::thread zbar_t(&Imgproc::scan_image_orb_zbar, this, converted_image);
        zbar_t.detach();
        m_zbar_enabled = true;
    }
}
