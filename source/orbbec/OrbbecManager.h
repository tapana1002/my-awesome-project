/* ========================================
*
* Copyright Daimler, 2017
* All Rights Reserved
* UNPUBLISHED, LICENSED SOFTWARE.
*
* CONFIDENTIAL AND PROPRIETARY INFORMATION
* WHICH IS THE PROPERTY OF Daimler.
*
* ========================================
*/
#ifndef _ORBBECMANAGER_H_
#define _ORBBECMANAGER_H_

#include "Common.h"
#include "Viewer.h"
#include "COBDevice.h"
#include <OpenNI.h>
#include <OniEnums.h>

/**	
*	High Level View of the OPENNI API
*	1) openni::OpenNI - Provides a single static entry point to the API. Also provides access to
*	devices, device related events, version and error information. Required to enable you to
*	connect to a Device.
*
*	2) openni::Device – Provides an interface to a single sensor device connected to the
*	system. Requires OpenNI to be initialized before it can be created. Devices provide access to
*	Streams.
*
*	3) openni::VideoStream – Abstracts a single video stream. Obtained from a specific
*	Device. Required to obtain VideoFrameRefs.
*
*	4) openni::VideoFrameRef – Abstracts a single video from and related meta-data.
*	Obtained from a specific Stream.
*/

/**
*	An enum
*	camera working status definition
*/
typedef enum{
	ORBBEC_INIT,
	ORBBEC_RUN,
	ORBBEC_STOP,
	ORBBEC_ERROR,
	ORBBEC_NONE
}CAMERA_STATUS;

/**
*	Each camera device has a unique URI which can be read from openni api.
*/
typedef struct camera_pair_list
{
	OrbbecCamera* 			camera;
	const char* 			uri;
	char 					serialNum[12];
	uvc_device_t* 			uvc_orbbec_device; //Only for Astra Pros
	uvc_device_handle_t*	uvc_dev_handle; //Only for Astra Pros
	uvc_stream_ctrl_t 		ctrl;
	CAMERA_STATUS 	camera_status;
}Camera_Pairs;

class OrbbecManager{
	public:
		OrbbecManager();
		virtual ~OrbbecManager();
		void orbbec_manager_loop();

		uvc_context_t 			*m_ctx;
		uvc_device_t 			**m_uvc_astra_list; 
		openni::Device 			m_orbbec_devices[MAX_CAMERA_NUM];
		openni::VideoStream 	m_depth_streams[MAX_CAMERA_NUM];
		openni::VideoStream 	m_color_streams[MAX_CAMERA_NUM];
		openni::VideoStream 	m_ir_streams[MAX_CAMERA_NUM];
		openni::VideoStream** 	m_openni_streams;
		openni::VideoFrameRef 	m_depth_frame[MAX_CAMERA_NUM];
		openni::VideoFrameRef 	m_color_frame[MAX_CAMERA_NUM];
		openni::VideoFrameRef 	m_ir_frame[MAX_CAMERA_NUM];
		openni::Array<openni::DeviceInfo> m_deviceList;

	private:
		bool init_orbbec_threads();
		bool start_orbbec_streams();
		bool capture_orbbec_streams();

		int 		 m_device_num;
		int          m_active_depth_stream_index;
		int 		 m_openni_stream_count;
		int 		 m_depth_stream_count;
		int 		 m_color_stream_count;
		int 		 m_ir_stream_count;
		Camera_Pairs m_camera_pairs[MAX_CAMERA_NUM];
		std::mutex   m_frame_mutexs[MAX_CAMERA_NUM];
};

/*
 * \Descr temporarily initialize openNI to get camera count
 * \param1
 */
int  get_orbbec_camera_count();

#endif //_ORBBECMANAGER_H_