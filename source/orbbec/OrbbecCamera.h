/* ========================================
*
* Copyright Daimler, 2017
* All Rights Reserved
* UNPUBLISHED, LICENSED SOFTWARE.
*
* CONFIDENTIAL AND PROPRIETARY INFORMATION
* WHICH IS THE PROPERTY OF Daimler.
*
* ========================================
*/
#ifndef _ORBBECCAMERA_H_
#define _ORBBECCAMERA_H_

#include "Common.h"
#include "Viewer.h"
#include <OpenNI.h>
#include <OniEnums.h>


class OrbbecCamera{
	public:
		OrbbecCamera(const char* uri, std::mutex&  m_frame_mtx, openni::VideoFrameRef& dframe,
					 openni::VideoFrameRef& cframe, openni::VideoFrameRef& iframe,
					 openni::VideoStream& depth_ref, openni::VideoStream& ir_ref, openni::VideoStream& rgb_ref);
		virtual ~OrbbecCamera();
		
		void  switchLaser(const char* uri, bool turnOn);
		int   get_camera_id()			{return m_camera_id;};
		void  set_camera_id(int id)	{m_camera_id = id;};
		const char* get_uri() 		{return m_camera_uri;};

		void 	get_current_depth_frame(openni::VideoFrameRef& frame_copy);
		void 	get_current_color_frame(openni::VideoFrameRef& frame_copy);
		void 	get_current_ir_frame(openni::VideoFrameRef& frame_copy);
		bool	set_current_depth_frame(openni::VideoFrameRef frame);
		bool	set_current_color_frame(openni::VideoFrameRef frame);
		bool	set_current_ir_frame(openni::VideoFrameRef frame);

    	void 	camera_loop();

    	const char* m_camera_serial;
    	
    	void start_camera_opengl_thread(){
        	m_opengl_thread = std::thread(&OrbbecCamera::opengl_glut_loop,this);
    	}

    	std::thread m_camera_thread;
    	bool 		m_zbar_enabled;

	private:
		int 					m_camera_id;
		const char* 			m_camera_uri;
		long unsigned int 		m_thread_id;
		long unsigned int 		m_glut_thread_id;
		uint8_t                 m_driver_cmdbuf1[10];
    	uint8_t                 m_driver_cmdbuf2[10];
		std::thread 			m_opengl_thread;
		std::mutex&  			m_frame_mtx_ref;
		openni::VideoFrameRef& 	m_depth_frame_ref;
		openni::VideoFrameRef 	m_curr_depth_frame_copy;
		openni::VideoFrameRef& 	m_color_frame_ref;
		openni::VideoFrameRef 	m_curr_color_frame_copy;
		openni::VideoFrameRef& 	m_ir_frame_ref;
		openni::VideoFrameRef 	m_curr_ir_frame_copy;
		openni::VideoStream&	m_depth_stream_ref;
		openni::VideoStream&	m_ir_stream_ref;
		openni::VideoStream&	m_rgb_stream_ref;

		void opengl_glut_loop();
		
		void save_frames_2_video();
		void save_depth_frame_csv();
		void save_point_cloud_csv();
		void save_point_cloud_color_csv();
		void save_depth_frame_json();
		void save_point_cloud_json();
		void save_point_cloud_color_json();
		void save_point_cloud_ply();
		void save_point_cloud_color_ply();
		void save_depth_image();
		void save_rgb_image();
		void save_ir_image();
		void save_rgb_video();
		void zbar_scan_color();
		void process_frame();
		void record_frame();
};


#endif 


