/* ========================================
*
* Copyright Daimler, 2017
* All Rights Reserved
* UNPUBLISHED, LICENSED SOFTWARE.
*
* CONFIDENTIAL AND PROPRIETARY INFORMATION
* WHICH IS THE PROPERTY OF Daimler.
*
* ========================================
*/
#ifndef _COMMON_H_
#define _COMMON_H_

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <iostream>
#include <fstream>
#include <memory>
#include <pthread.h>
#include <algorithm>
#include <tuple>
#include <thread>
#include <vector>
#include <mutex>
#include <string>
#include <opencv/cv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/imgproc.hpp>
#include <OpenNI.h>
#include <pthread.h>
#include <easylogging.h>
#include <cstring>
#include <jsoncpp/json/json.h>
#include <jsoncpp/json/writer.h>
#include <libusb-1.0/libusb.h>
#include <libuvc/libuvc.h>
#include <zbar.h>

#ifndef TRUE
#define TRUE	(1)
#endif 
#ifndef FALSE
#define FALSE	(0)
#endif 
#ifndef ENABLE
#define ENABLE	(1)
#endif 

#ifndef DISABLE
#define	DISABLE	(0)
#endif 


#define FRAME_WIDTH			(640)	
#define FRAME_HIGHT			(480)
#define PIXEL_NUM			(FRAME_WIDTH*FRAME_HIGHT)

#define ORBBEC_RECORDING	(DISABLE)		
#define CONVERT_2_CSV 		(ENABLE)

#define DEPTH_FRAME_RCVD	(0x01)	
#define COLOR_FRAME_RCVD	(0x02)

#define TIME_STAMP_NEEDED	(TRUE)
#define OPENCV_EXAMPLE		(FALSE)

/**
*	Configure Network Streaming Server and Client 
*/
#define COROS_STREAM_SERVER		(0)
#define COROS_STREAM_CLIENT		(1)
#define COROS_STREAM 			(COROS_STREAM_SERVER)

/**
*	Maximum number of the camera devices in CoROS system 
*/
#define MAX_CAMERA_NUM	(10)


typedef signed char int8;
typedef short int16;
typedef int int32;

typedef unsigned char u08;
typedef unsigned short int u16;
typedef unsigned int u32;

/*
*	\define command line arguments global variables.
*/
typedef struct
{
	int 	 argc; 
	char 	 **argv;
	bool 	 rgb_enabled;
	bool 	 ir_enabled;
	bool 	 rgb_depth_enabled;
	bool 	 csv_depth_record_enabled;
	bool 	 csv_pointcloud_record_enabled;
	bool 	 csv_pointcloudcolor_record_enabled;
	bool 	 json_depth_record_enabled;
	bool 	 json_pointcloud_record_enabled;
	bool 	 json_pointcloudcolor_record_enabled;
	bool 	 ply_pointcloud_record_enabled;
	bool 	 ply_pointcloudcolor_record_enabled;
	bool 	 rgb_save_jpeg_enabled;
	bool 	 depth_save_jpeg_enabled;
	bool 	 ir_save_jpeg_enabled;
	bool 	 rgb_save_avi_enabled;
	bool 	 zbar_enabled;
	bool 	 basic_cv_mode_enabled;
	bool 	 simple_box_tracker_demo_enabled;
}COMMAND_LINE_ARGS;


/*
*	\ThreadJoiner will be used safely join the thread: Resource Acquisition Is Initialization (RAII)
*	\This is to make sure join will happen even when there is exception occurs and can also make sure join happens at the end function.
*	\Example: 
*			std::thread t(thread_function); 
*			ThreadJoiner thread_joiner(t); 
*	\Other than use t.join();
*/
class ThreadJoiner 
{
	public:
		explicit ThreadJoiner(std::thread& t):m_thread(t){}
		~ThreadJoiner() 
		{
			if(m_thread.joinable()) 
			{
				m_thread.join();
			}
		}

	private:
		std::thread& m_thread;
};


#endif //_COMMON_H_

