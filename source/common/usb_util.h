/* ========================================
*
* Copyright Daimler, 2017
* All Rights Reserved
* UNPUBLISHED, LICENSED SOFTWARE.
*
* CONFIDENTIAL AND PROPRIETARY INFORMATION
* WHICH IS THE PROPERTY OF Daimler.
*
* ========================================
*/
#ifndef _USB_UTIL_H_
#define _USB_UTIL_H_

#include <Common.h>

/* We define the namespace incase we use a function name
 thats the same as an outside library.*/
namespace corosusb
{
	const char * libusb_err(int err);
	int 		 libusb_readinfo();
}


#endif