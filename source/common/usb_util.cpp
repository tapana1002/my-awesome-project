/* ========================================
*
* Copyright Daimler, 2017
* All Rights Reserved
* UNPUBLISHED, LICENSED SOFTWARE.
*
* CONFIDENTIAL AND PROPRIETARY INFORMATION
* WHICH IS THE PROPERTY OF Daimler.
*
* ========================================
*/

#include "usb_util.h"

// static int print_device(libusb_device *dev, int level)
// {
// 	struct libusb_device_descriptor desc;
// 	libusb_device_handle *handle = NULL;
// 	char description[256];
// 	unsigned char string[256];
// 	int ret, i;

// 	int verbose = 1;

// 	ret = libusb_get_device_descriptor(dev, &desc);
// 	if (ret < 0) {
// 		fprintf(stderr, "failed to get device descriptor");
// 		return -1;
// 	}

// 	ret = libusb_open(dev, &handle);
// 	if (LIBUSB_SUCCESS == ret) {
// 		if (desc.iManufacturer) {
// 			ret = libusb_get_string_descriptor_ascii(handle, desc.iManufacturer, string, sizeof(string));
// 			if (ret > 0)
// 			{
// 				snprintf(description, sizeof(description), "%s - ", string);
// 				snprintf(description, sizeof(description), "%04X - ", desc.idVendor);
// 			} else
// 			{
// 				snprintf(description, sizeof(description), "%04X - ", desc.idVendor);
// 			}
// 		}
// 		else
// 			snprintf(description, sizeof(description), "%04X - ", desc.idVendor);

// 		if (desc.iProduct) {
// 			ret = libusb_get_string_descriptor_ascii(handle, desc.iProduct, string, sizeof(string));
// 			if (ret > 0)
// 			{
// 				snprintf(description + strlen(description), sizeof(description) -
// 				strlen(description), "%s", string);
// 				snprintf(description + strlen(description), sizeof(description) -
// 				strlen(description), "%04X", desc.idProduct);
// 			} else
// 			{
// 				snprintf(description + strlen(description), sizeof(description) -
// 				strlen(description), "%04X", desc.idProduct);
// 			}
// 		}
// 		else
// 			snprintf(description + strlen(description), sizeof(description) -
// 			strlen(description), "%04X", desc.idProduct);
// 		if (desc.iSerialNumber) {
// 			ret = libusb_get_string_descriptor_ascii(handle, desc.iSerialNumber, string, sizeof(string));
// 			if (ret > 0)
// 				printf("%.*s  - Serial Number: %s\n", level * 2,
// 				"                    ", string);
// 		}
// 	}
// 	else {
// 		snprintf(description, sizeof(description), "%04X - %04X",
// 			desc.idVendor, desc.idProduct);
// 	}

// 	printf("%.*sDev (bus %d, device %d): %s\n", level * 2, "                    ",
// 		libusb_get_bus_number(dev), libusb_get_device_address(dev), description);

// 	if (verbose) {
// 		for (i = 0; i < desc.bNumConfigurations; i++) {
// 			struct libusb_config_descriptor *config;
// 			ret = libusb_get_config_descriptor(dev, i, &config);
// 			if (LIBUSB_SUCCESS != ret) {
// 				printf("  Couldn't retrieve descriptors\n");
// 				continue;
// 			}

// 			libusb_free_config_descriptor(config);
// 		}
// 	}

// 	if (handle)
// 		libusb_close(handle);

// 	return 0;
// }

int corosusb::libusb_readinfo()
{
	libusb_device **devs;
	int ret_code;
	ssize_t usb_device_count = 0;

	/* Initialize libusb for USB device management*/
	ret_code = libusb_init(NULL);
	if(ret_code < 0)
	{
		LOG(ERROR) << "libusb initialize failure error: " << corosusb::libusb_err(ret_code);
		return -1;
	}	

	usb_device_count = libusb_get_device_list(NULL, &devs);
	if(usb_device_count < 0)
	{	
		LOG(ERROR) << "libusb get list failure!";
		return -1;
	}

	// LOG(INFO) << "Found this many usb devices: " << usb_device_count;
	// for (int i = 0; devs[i]; ++i) {
	// 	print_device(devs[i], 0);
	// }

	libusb_free_device_list(devs, 1);

	return 0;
}

const char * corosusb::libusb_err(int err)
{
	switch(err)
	{
		case 0:
			return "LIBUSB_SUCCESS";
		case -1:
			return "LIBUSB_ERROR_IO";
		case -2:
			return "LIBUSB_ERROR_INVALID_PARAM";
		case -3:
			return "LIBUSB_ERROR_ACCESS";
		case -4:
			return "LIBUSB_ERROR_NO_DEVICE";
		case -5:
			return "LIBUSB_ERROR_NOT_FOUND";
		case -6:
			return "LIBUSB_ERROR_BUSY";
		case -7:
			return "LIBUSB_ERROR_TIMEOUT";
		case -8:
			return "LIBUSB_ERROR_OVERFLOW";
		case -9:
			return "LIBUSB_ERROR_PIPE";
		case -10:
			return "LIBUSB_ERROR_INTERRUPTED";
		case -11:
			return "LIBUSB_ERROR_NO_MEM";
		case -12:
			return "LIBUSB_ERROR_NOT_SUPPORTED";
		case -99:
			return "LIBUSB_ERROR_OTHER";							
		default:
			return "LIBUSB_ERROR_UNDEFINED";
	}
}