/*************************************************************************
	> File Name: realsenseCamera.cpp
	> Author: 
	> Mail: 
	> Created Time: Mon 22 May 2017 05:13:20 PM PDT
 ************************************************************************/
#include "RealsenseCamera.h"
#include "CoROS.h"
#include "Common.h"

extern COMMAND_LINE_ARGS g_command_line_args;

/**
*   Contructor for RealsenseCamera
*   RealsenseCamera and RealsenseManager use the same buffer/memory location for storing camera specific data
*   Let Realsense objects pointers point to the corresponding location of RealsenseManager objects
*/
RealsenseCamera::RealsenseCamera(CameraInfo* camera, std::mutex* mtx):m_camera(nullptr),m_win(nullptr)
{
    m_camera = camera; 
    m_mutexs_ref = mtx;
}

/**
*   Destructor for RealsenseCamera
*   Delete the allocated memories
*/
RealsenseCamera::~RealsenseCamera()
{
    delete m_camera;
    delete m_mutexs_ref;
}

/**
*   camera_loop
*/
void RealsenseCamera::camera_loop()
{ 
    sleep(1);
    while(1)
    {
        record_frame();
        //display_frame();
    }
}

/**
*   record_frame
*   Record frames into files of different formats
*/
void RealsenseCamera::record_frame()
{
    if(g_command_line_args.csv_depth_record_enabled)
    {   
        save_depth_frame_csv();
    } 
    else if(g_command_line_args.csv_pointcloud_record_enabled)
    {
        save_point_cloud_csv();
    }
    else if(g_command_line_args.csv_pointcloudcolor_record_enabled)
    {
        save_point_cloud_color_csv();
    }
    else if(g_command_line_args.json_depth_record_enabled)
    {
        save_depth_frame_json();
    }
    else if(g_command_line_args.json_pointcloud_record_enabled)
    {
        save_point_cloud_json();
    }
    else if(g_command_line_args.json_pointcloudcolor_record_enabled)
    {
        save_point_cloud_color_json();
    }
    else if(g_command_line_args.rgb_save_jpeg_enabled)
    {
        save_rgb_image();
    }
    else if(g_command_line_args.depth_save_jpeg_enabled)
    {
        save_depth_image(); 
    }
    else if(g_command_line_args.ir_save_jpeg_enabled)
    {
        save_ir_image();
    }
    else if(g_command_line_args.rgb_save_avi_enabled)
    {
        save_rgb_video();
    }
    else if(g_command_line_args.zbar_enabled && !m_zbar_enabled)
    {
        zbar_scan_color();
    }
    else if(g_command_line_args.ply_pointcloud_record_enabled)
    {
        save_point_cloud_ply();
    }
    else if(g_command_line_args.ply_pointcloudcolor_record_enabled)
    {
        save_point_cloud_color_ply();
    }
}

/**
*   save_depth_frame_csv 
*   save depth info to csv file
*   Note: The depth data is the raw data get from camera device, could be (0,65535)
*/
void RealsenseCamera::save_depth_frame_csv()
{
    static int frame_cnts = 0;
    std::string image_name_csv = "rs-" + std::to_string(m_camera->m_camera_id) + "-depth-" + std::to_string(frame_cnts) + 
                            #if TIME_STAMP_NEEDED
                                "-" + std::to_string(m_camera->m_time_stamp) + "ms" + 
                            #endif
                                ".csv";
    std::ofstream csv_file(image_name_csv);
    
    int imageHeight = m_camera->m_intrin_depth.height;
    int imageWidth  = m_camera->m_intrin_depth.width;

    for(int dy=0; dy<imageHeight; ++dy)
    {
        for(int dx=0; dx<imageWidth; ++dx)
        {
            int idx = dx + dy * imageWidth;

            // Retrieve the 16-bit depth value and map it into a depth in meters
            m_mutexs_ref->lock();
            uint16_t depth_value = m_camera->m_depth_image[idx];
            m_mutexs_ref->unlock();

            //float depth_in_meters = depth_value * m_camera->m_depth_scale;

            // Skip over pixels with a depth value of zero, which is used to indicate no data
            if(depth_value == 0) continue;
            
            csv_file << std::to_string(dx) << "," << std::to_string(dy) << "," << std::to_string(depth_value) << "\n";
        }
    }
    csv_file.close();
    frame_cnts++;
}

/**
*   save_point_cloud_csv 
*   save point cloud info to csv file
*   Note: The point cloud here is the calculated/converted 3d value with regards to the world coordinate
*/
void RealsenseCamera::save_point_cloud_csv()
{
    static int frame_cnts = 0;
    std::string point_cloud_name_csv = "rs-" + std::to_string(m_camera->m_camera_id) + "-point-cloud-" + std::to_string(frame_cnts) + 
                                #if TIME_STAMP_NEEDED
                                    "-" + std::to_string(m_camera->m_time_stamp) + "ms" + 
                                #endif
                                    ".csv";
                                
    std::ofstream csv_file(point_cloud_name_csv);
    
    int imageHeight = m_camera->m_intrin_depth.height;
    int imageWidth  = m_camera->m_intrin_depth.width;

    for(int idx=0; idx<imageHeight*imageWidth; ++idx)
    {
        // Retrieve the 16-bit depth value and map it into a depth in meters
        m_mutexs_ref->lock();
        rs::float3 point = m_camera->m_point_cloud[idx];
        m_mutexs_ref->unlock();

        if(point.z == 0) continue;

        // Skip over pixels with a depth value of zero, which is used to indicate no data
        csv_file << std::to_string(point.x) << "," << std::to_string(point.y) << "," << std::to_string(point.z) << "\n";
    }
    csv_file.close();
    frame_cnts++;
}

/**
*   save_point_cloud_color_csv 
*   save point cloud data combined with color information to csv file
*   Note: The point cloud here is the calculated/converted 3d value with regards to the world coordinate
*/
void RealsenseCamera::save_point_cloud_color_csv()
{
    static int frame_cnts = 0;
    std::string point_cloud_name_csv = "rs-" + std::to_string(m_camera->m_camera_id) + "-point-cloud-color-" + std::to_string(frame_cnts) + 
                                #if TIME_STAMP_NEEDED
                                    "-" + std::to_string(m_camera->m_time_stamp) + "ms" + 
                                #endif
                                    ".csv";
                                
    std::ofstream csv_file(point_cloud_name_csv);
    
    int imageHeight = m_camera->m_intrin_depth.height;
    int imageWidth  = m_camera->m_intrin_depth.width;
    uint64_t color_idx = 0;
    for(int idx=0; idx<imageHeight*imageWidth; ++idx)
    {
        // Retrieve the 16-bit depth value and map it into a depth in meters
        m_mutexs_ref->lock();
        rs::float3 point = m_camera->m_point_cloud[idx];
        m_camera->m_intrin_color.project_to_texcoord(m_camera->m_extrin.transform(point));
        uint8_t r_value = m_camera->m_color_image[color_idx];
        color_idx++;
        uint8_t g_value = m_camera->m_color_image[color_idx];
        color_idx++;
        uint8_t b_value = m_camera->m_color_image[color_idx];
        color_idx++;
        m_mutexs_ref->unlock();

        if(point.z == 0) 
        {
            continue;
        }

        // Skip over pixels with a depth value of zero, which is used to indicate no data
        csv_file << std::to_string(point.x) << "," << std::to_string(point.y) << "," << std::to_string(point.z) << "," <<
        std::to_string(r_value) << "," << std::to_string(g_value) << ","<< std::to_string(b_value) << "\n";
        
    }
    csv_file.close();
    frame_cnts++;
}

/**
*   save_depth_frame_json 
*   save depth info to json file
*   Note: Save raw depth data into json file
*/
void RealsenseCamera::save_depth_frame_json()
{
    static int frame_cnts = 0;
    std::string image_name_json = "rs-" + std::to_string(m_camera->m_camera_id) + "-depth-" + std::to_string(frame_cnts) + 
                            #if TIME_STAMP_NEEDED
                                "-" + std::to_string(m_camera->m_time_stamp) + "ms" + 
                            #endif
                                ".json";
    std::ofstream json_image(image_name_json);
    
    int imageHeight = m_camera->m_intrin_depth.height;
    int imageWidth  = m_camera->m_intrin_depth.width;

    Json::Value depth_frame; 
    depth_frame["timestamp"] = std::to_string(m_camera->m_time_stamp); 
    depth_frame["cameraId"] = m_camera->m_camera_id;
    depth_frame["imageWidth"] = m_camera->m_stream_width;
    depth_frame["imageHeight"] = m_camera->m_stream_height;

    for(int dy=0; dy<imageHeight; ++dy)
    {
        for(int dx=0; dx<imageWidth; ++dx)
        {
            int idx = dx + dy * imageWidth;

            // Retrieve the 16-bit depth value and map it into a depth in meters
            m_mutexs_ref->lock();
            uint16_t depth_value = m_camera->m_depth_image[idx];
            m_mutexs_ref->unlock();

            // Skip over pixels with a depth value of zero, which is used to indicate no data
            if(depth_value == 0) continue;

            std::string pixel_name = "p" + std::to_string(idx);  
            depth_frame["imagePixelData"][pixel_name]["x"] = dy;
            depth_frame["imagePixelData"][pixel_name]["y"] = dx;
            depth_frame["imagePixelData"][pixel_name]["i"] = (dy*640)+dx;
            depth_frame["imagePixelData"][pixel_name]["z"] = depth_value; 
        }
    }
    json_image << depth_frame << std::endl;
    json_image.close();

    frame_cnts++;
}
/**
*   save_point_cloud_json 
*   save point cloud info to json file
*   Note: The point cloud here is the calculated/converted 3d value with regards to the world coordinate
*/
void RealsenseCamera::save_point_cloud_json()
{
    static int frame_cnts = 0;
    std::string point_cloud_name_json = "rs-" + std::to_string(m_camera->m_camera_id) + "-point-cloud-" + std::to_string(frame_cnts) + 
                            #if TIME_STAMP_NEEDED
                                "-" + std::to_string(m_camera->m_time_stamp) + "ms" + 
                            #endif
                                ".json";
    std::ofstream json_image(point_cloud_name_json);
    
    int imageHeight = m_camera->m_intrin_depth.height;
    int imageWidth  = m_camera->m_intrin_depth.width;

    Json::Value depth_frame; 
    depth_frame["timestamp"] = std::to_string(m_camera->m_time_stamp); 
    depth_frame["cameraId"] = m_camera->m_camera_id;
    depth_frame["imageWidth"] = m_camera->m_stream_width;
    depth_frame["imageHeight"] = m_camera->m_stream_height;;

    for(int dy=0; dy<imageHeight; ++dy)
    {
        for(int dx=0; dx<imageWidth; ++dx)
        {
            int idx = dx + dy * imageWidth;

            // Retrieve the 16-bit depth value and map it into a depth in meters
            m_mutexs_ref->lock();
            rs::float3 point = m_camera->m_point_cloud[idx];
            m_mutexs_ref->unlock();

            // Skip over pixels with a depth value of zero, which is used to indicate no data
            if(point.z == 0) continue;

            std::string pixel_name = "p" + std::to_string(idx);  
            depth_frame["imagePixelData"][pixel_name]["x"] = point.x;
            depth_frame["imagePixelData"][pixel_name]["y"] = point.y;
            depth_frame["imagePixelData"][pixel_name]["z"] = point.z;  
            depth_frame["imagePixelData"][pixel_name]["i"] = (dy*640)+dx;
        }
    }

    json_image << depth_frame << std::endl;
    json_image.close();
    frame_cnts++;
}
/**
*   save_point_cloud_color_json 
*   save point cloud data combined to color data to json file
*   Note: The point cloud here is the calculated/converted 3d value with regards to the world coordinate
*/
void RealsenseCamera::save_point_cloud_color_json()
{
    static int frame_cnts = 0;
    std::string point_cloud_name_json = "rs-" + std::to_string(m_camera->m_camera_id) + "-point-cloud-color-" + std::to_string(frame_cnts) + 
                            #if TIME_STAMP_NEEDED
                                "-" + std::to_string(m_camera->m_time_stamp) + "ms" + 
                            #endif
                                ".json";
    std::ofstream json_image(point_cloud_name_json);
    
    int imageHeight = m_camera->m_intrin_depth.height;
    int imageWidth  = m_camera->m_intrin_depth.width;

    Json::Value depth_frame; 
    depth_frame["timestamp"] = std::to_string(m_camera->m_time_stamp); 
    depth_frame["cameraId"] = m_camera->m_camera_id;
    depth_frame["imageWidth"] = m_camera->m_stream_width;
    depth_frame["imageHeight"] = m_camera->m_stream_height;;

    uint64_t color_idx = 0;
    for(int dy=0; dy<imageHeight; ++dy)
    {
        for(int dx=0; dx<imageWidth; ++dx)
        {
            int idx = dx + dy * imageWidth;

            // Retrieve the 16-bit depth value and map it into a depth in meters
            m_mutexs_ref->lock();
            rs::float3 point = m_camera->m_point_cloud[idx];
            m_camera->m_intrin_color.project_to_texcoord(m_camera->m_extrin.transform(point));
            uint8_t r_value = m_camera->m_color_image[color_idx];
            color_idx++;
            uint8_t g_value = m_camera->m_color_image[color_idx];
            color_idx++;
            uint8_t b_value = m_camera->m_color_image[color_idx];
            color_idx++;
            m_mutexs_ref->unlock();

            // Skip over pixels with a depth value of zero, which is used to indicate no data
            if(point.z == 0) continue;

            std::string pixel_name = "p" + std::to_string(idx);  
            depth_frame["imagePixelData"][pixel_name]["x"] = point.x;
            depth_frame["imagePixelData"][pixel_name]["y"] = point.y;
            depth_frame["imagePixelData"][pixel_name]["z"] = point.z; 
            depth_frame["imagePixelData"][pixel_name]["r"] = r_value;
            depth_frame["imagePixelData"][pixel_name]["g"] = g_value;
            depth_frame["imagePixelData"][pixel_name]["b"] = b_value;
            depth_frame["imagePixelData"][pixel_name]["i"] = (dy*640)+dx;
        }
    }

    json_image << depth_frame << std::endl;
    json_image.close();
    frame_cnts++;
}


void RealsenseCamera::save_point_cloud_ply()
{
    static int frame_cnts = 0;
    std::string ply_header;
    int point_count = 0;
    u16 img_copy[m_depth_image_size] = {0};

    memset(img_copy,0,sizeof(img_copy));

    m_mutexs_ref->lock();
    memcpy(img_copy, m_camera->m_depth_image, m_depth_image_size * sizeof(u16) );
    m_mutexs_ref->unlock();

    std::string point_cloud_name_ply = "rs-" + std::to_string(m_camera->m_camera_id) + "-point-cloud-" + std::to_string(frame_cnts) + 
                                #if TIME_STAMP_NEEDED
                                    "-" + std::to_string(m_camera->m_time_stamp) + "ms" + 
                                #endif
                                    ".ply";
                                
    std::ofstream ply_file(point_cloud_name_ply);
    
    int imageHeight = m_camera->m_intrin_depth.height;
    int imageWidth  = m_camera->m_intrin_depth.width;

    std::string point_cloud_points;
    for(int dy=0; dy<imageHeight; ++dy)
    {
        for(int dx=0; dx<imageWidth; ++dx)
        {
            int idx = dx + dy * imageWidth;
            point_count++;

            // Retrieve the 16-bit depth value and map it into a depth in meters
            
            uint16_t depth_value = img_copy[idx];
            float depth_in_meters = depth_value * m_camera->m_depth_scale;
            rs::float2 depth_pixel = {(float)dx, (float)dy};
            rs::float3 depth_point = m_camera->m_intrin_depth.deproject(depth_pixel, depth_in_meters);
            

            if(depth_value == 0)
            {
                point_count--;
                continue;
            }
            std::string row = std::to_string(depth_point.x) + " " + std::to_string(depth_point.y) + " " + std::to_string(depth_point.z) + "\n";
            point_cloud_points.append(row);
        }
    }

    ply_header = "ply\nformat ascii 1.0\nelement vertex " + std::to_string(point_count) + "\nproperty float32 x\n"
                + "property float32 y\nproperty float32 z\nelement face 0\nproperty list uint8 int32 vertex_indices\n" + "end_header\n";
    
    ply_file << ply_header;
    ply_file << point_cloud_points;
    
    ply_file.close();
    frame_cnts++;
}

void RealsenseCamera::save_point_cloud_color_ply()
{
    static int frame_cnts = 0;

    std::string ply_header;
    int point_count = 0;

    std::string point_cloud_name_ply = "rs-" + std::to_string(m_camera->m_camera_id) + "-point-cloud-color-" + std::to_string(frame_cnts) + 
                                #if TIME_STAMP_NEEDED
                                    "-" + std::to_string(m_camera->m_time_stamp) + "ms" + 
                                #endif
                                    ".ply";
                                
    std::ofstream ply_file(point_cloud_name_ply);

    int imageHeight = m_camera->m_intrin_depth.height;
    int imageWidth  = m_camera->m_intrin_depth.width;

    std::string point_cloud_points;
    int cy = 0;
    int cx = 0;
    for(int dy=0; dy<imageHeight; ++dy)
    {
        for(int dx=0; dx<imageWidth; ++dx)
        {
            point_count++;
            m_mutexs_ref->lock();
            uint16_t depth_value = m_camera->m_depth_image[dy * imageWidth + dx];
            uint8_t r_value = m_camera->m_color_image[cy * imageWidth + cx];
            cx++;
            uint8_t g_value = m_camera->m_color_image[cy * imageWidth + cx];
            cx++;
            uint8_t b_value = m_camera->m_color_image[cy * imageWidth + cx];
            cx++;
            float depth_in_meters = depth_value * m_camera->m_depth_scale;
            rs::float2 depth_pixel = {(float)dx, (float)dy};
            rs::float3 depth_point = m_camera->m_intrin_depth.deproject(depth_pixel, depth_in_meters);
            rs::float3 color_point = m_camera->m_extrin.transform(depth_point);
            m_mutexs_ref->unlock();

            if(depth_value == 0)
            {
                point_count--;
                continue;
            }
            std::string row = std::to_string(color_point.x) + " " + std::to_string(color_point.y) + " " + std::to_string(color_point.z) + " "
                            + std::to_string(r_value) + " " + std::to_string(g_value) + " "+ std::to_string(b_value) + "\n";
            point_cloud_points.append(row);
                
        }
        cy++;
        cx=0;
    }
    ply_header = "ply\nformat ascii 1.0\nelement vertex " + std::to_string(point_count) + "\nproperty float32 x\n"
                + "property float32 y\nproperty float32 z\nproperty uchar r\nproperty uchar g\nproperty uchar b" 
                + "\nelement face 0\nproperty list uint8 int32 vertex_indices\n" + "end_header\n";
    
    ply_file << ply_header;
    ply_file << point_cloud_points;

    ply_file.close();
    frame_cnts++;
}
/**
*   save_depth_image
*   Save depth frames to jpeg format in 2D
*/
void RealsenseCamera::save_depth_image()
{
    static int frame_cnts = 0;
    cv::Mat depth_image(480, 640, CV_16UC1);
    cv::Mat d_image;
    int step = depth_image.step;

    std::vector<int> compression_params; //vector that stores the compression parameters of the image
    compression_params.push_back(CV_IMWRITE_JPEG_QUALITY); //specify the compression technique
    compression_params.push_back(100); //specify the compression quality

    if(nullptr != m_camera->m_depth_image)
    {
        std::string depth_image_name = "rs-" + std::to_string(m_camera->m_camera_id) + "-depth-" + std::to_string(frame_cnts) + 
                                    #if TIME_STAMP_NEEDED
                                        "-" + std::to_string(m_camera->m_time_stamp) + "ms" + 
                                    #endif
                                        ".jpeg";
        m_mutexs_ref->lock();
        memcpy(depth_image.data, m_camera->m_depth_image, step*480);
        m_mutexs_ref->unlock();

        depth_image.convertTo(d_image,CV_8UC1);
        cv::equalizeHist( d_image, d_image );
        cv::applyColorMap(d_image, d_image, cv::COLORMAP_WINTER);

        cv::imwrite(depth_image_name, d_image, compression_params);
        frame_cnts++;
    }
}
/**
*   save_frame_2_photo
*   Save color frames to jpeg photos
*/
void RealsenseCamera::save_rgb_image()
{
    static int frame_cnts = 0;

    std::vector<int> compression_params; //vector that stores the compression parameters of the image
    compression_params.push_back(CV_IMWRITE_JPEG_QUALITY); //specify the compression technique
    compression_params.push_back(100); //specify the compression quality
    
    if(nullptr != m_camera->m_color_image)
    {
        std::string color_image_name = "rs-" + std::to_string(m_camera->m_camera_id) + "-color-" + std::to_string(frame_cnts) + 
                                    #if TIME_STAMP_NEEDED
                                        "-" + std::to_string(m_camera->m_time_stamp) + "ms" + 
                                    #endif
                                        ".jpeg";
        m_mutexs_ref->lock();
        cv::Mat color_image(cv::Size(1920, 1080), CV_8UC3, (void*)m_camera->m_color_image, cv::Mat::AUTO_STEP);  
        m_mutexs_ref->unlock();
        cv::Mat converted_image;
        cv::cvtColor(color_image, converted_image, CV_BGR2RGB);
        cv::imwrite(color_image_name, converted_image, compression_params);
        frame_cnts++;
    }
}

void RealsenseCamera::save_ir_image()
{
    static int frame_cnts = 0;
    std::vector<int> compression_params; //vector that stores the compression parameters of the image
    compression_params.push_back(CV_IMWRITE_JPEG_QUALITY); //specify the compression technique
    compression_params.push_back(100); //specify the compression quality

    if(nullptr != m_camera->m_infrared_image && nullptr != m_camera->m_infrared2_image)
    {
        std::string color_image_name1 = "rs-" + std::to_string(m_camera->m_camera_id) + "-ir-dev1-" + std::to_string(frame_cnts) + 
                                    #if TIME_STAMP_NEEDED
                                        "-" + std::to_string(m_camera->m_time_stamp) + "ms" + 
                                    #endif
                                        ".jpeg";
        std::string color_image_name2 = "rs-" + std::to_string(m_camera->m_camera_id) + "-ir-dev2-" + std::to_string(frame_cnts) + 
                                    #if TIME_STAMP_NEEDED
                                        "-" + std::to_string(m_camera->m_time_stamp) + "ms" + 
                                    #endif
                                        ".jpeg";
        m_mutexs_ref->lock();
        cv::Mat ir1(cv::Size(640, 480), CV_8UC1, (void*)m_camera->m_infrared_image, cv::Mat::AUTO_STEP);
        cv::Mat ir2(cv::Size(640, 480), CV_8UC1, (void*)m_camera->m_infrared2_image, cv::Mat::AUTO_STEP);
        m_mutexs_ref->unlock();

        //Apply Histogram Equalization
        cv::equalizeHist( ir1, ir1 );
        cv::applyColorMap(ir1, ir1, cv::COLORMAP_BONE);

        cv::equalizeHist( ir2, ir2 );
        cv::applyColorMap(ir2, ir2, cv::COLORMAP_BONE);

        cv::imwrite(color_image_name1, ir1, compression_params);
        cv::imwrite(color_image_name2, ir2, compression_params);
        frame_cnts++;
    }
}

void RealsenseCamera::save_rgb_video()
{
    static int video_cnts = 0;
    static cv::VideoWriter *video_ptr = NULL;

    std::string video_file_name = "rs-vid-" + std::to_string(m_camera->m_camera_id);
    video_file_name.append(std::to_string(video_cnts));
    video_file_name.append(".avi");

    if(nullptr != m_camera->m_color_image)
    {
        m_mutexs_ref->lock();
        cv::Mat color_image(cv::Size(1920, 1080), CV_8UC3, (void*)m_camera->m_color_image, cv::Mat::AUTO_STEP);  
        m_mutexs_ref->unlock();

        cv::Mat converted_image;
        cv::cvtColor(color_image, converted_image, CV_BGR2RGB);

        if(video_cnts == 0)
        {   
            video_ptr = new cv::VideoWriter(video_file_name, CV_FOURCC('M', 'J', 'P', 'G'), 30, cv::Size(converted_image.cols,converted_image.rows), true);
            video_cnts++;
        }
        video_ptr->write(converted_image);
    }
}

void RealsenseCamera::zbar_scan_color()
{

    if(nullptr != m_camera->m_color_image)
    {
        m_mutexs_ref->lock();
        cv::Mat color_image(cv::Size(1920, 1080), CV_8UC3, (void*)m_camera->m_color_image, cv::Mat::AUTO_STEP);  
        m_mutexs_ref->unlock();

        cv::Mat converted_image;
        cv::cvtColor(color_image, converted_image, CV_BGR2RGB);

        std::thread zbar_t(&Imgproc::scan_image_rs_zbar, this, converted_image);
        zbar_t.detach();
        m_zbar_enabled = true;
    }
}

/**
*   log_config
*   Realsense library provide log functions and it can be configured here
*/
void RealsenseCamera::log_config()
{
    //Turn on logging. We can separately enable logging to console or to file, and use different severity filters for each.
    rs::log_to_console(rs::log_severity::warn);
    rs::log_to_file(rs::log_severity::debug, "librealsense.log");
}

/**
*   The below functions are used only for rendering frame to display.
*   Right now we have issue with displaying frames from multiple cameras
*/

static double yaw, pitch, lastX, lastY; 
static int ml;

static void on_mouse_button(GLFWwindow * win, int button, int action, int mods)
{
    if(button == GLFW_MOUSE_BUTTON_LEFT) ml = action == GLFW_PRESS;
}

static double clamp(double val, double lo, double hi) 
{ 
    return val < lo ? lo : val > hi ? hi : val; 
}

static void on_cursor_pos(GLFWwindow * win, double x, double y)
{
    if(ml)
    {
        yaw = clamp(yaw - (x - lastX), -120, 120);
        pitch = clamp(pitch + (y - lastY), -80, 80);
    }
    lastX = x;
    lastY = y;
}

int RealsenseCamera::glfw_init()
{
    // Open a GLFW window to display our output
    glfwInit();
    m_win = glfwCreateWindow(1280, 960, "coros-realsense-r200", nullptr, nullptr);
    glfwSetCursorPosCallback(m_win, on_cursor_pos);
    glfwSetMouseButtonCallback(m_win, on_mouse_button);
    glfwMakeContextCurrent(m_win);
    return true;
}

void RealsenseCamera::set_perspective_transform()
{
    // Set up a perspective transform in a space that we can rotate by clicking and dragging the mouse
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60, (float)1280/960, 0.01f, 20.0f);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0,0,0, 0,0,1, 0,-1,0);
    glTranslatef(0,0,+0.5f);
    glRotated(pitch, 1, 0, 0);
    glRotated(yaw, 0, 1, 0);
    glTranslatef(0,0,-0.5f); 
}


void RealsenseCamera::render_3D_depth_data()
{
    // We will render our depth data as a set of points in 3D space
    glPointSize(2);
    glEnable(GL_DEPTH_TEST);
    glBegin(GL_POINTS);

    for(int dy=0; dy<m_camera->m_intrin_depth.height; ++dy)
    {
        for(int dx=0; dx<m_camera->m_intrin_depth.width; ++dx)
        {
            // Retrieve the 16-bit depth value and map it into a depth in meters
            m_mutexs_ref->lock();
            uint16_t depth_value = m_camera->m_depth_image[dy * m_camera->m_intrin_depth.width + dx];
            m_mutexs_ref->unlock();

            float depth_in_meters = depth_value * m_camera->m_depth_scale;

            // Skip over pixels with a depth value of zero, which is used to indicate no data
            if(depth_value == 0) continue;

            // Map from pixel coordinates in the depth image to pixel coordinates in the color image
            rs::float2 depth_pixel = {(float)dx, (float)dy};
            rs::float3 depth_point = m_camera->m_intrin_depth.deproject(depth_pixel, depth_in_meters);
            rs::float3 color_point = m_camera->m_extrin.transform(depth_point);
            rs::float2 color_pixel = m_camera->m_intrin_color.project(color_point);

            // Use the color from the nearest color pixel, or pure white if this point falls outside the color image
            const int cx = (int)std::round(color_pixel.x), cy = (int)std::round(color_pixel.y);
            if(cx < 0 || cy < 0 || cx >= m_camera->m_intrin_color.width || cy >= m_camera->m_intrin_color.height)
            {
                glColor3ub(255, 255, 255);
            }
            else
            {
                m_mutexs_ref->lock();
                glColor3ubv(m_camera->m_color_image + (cy * m_camera->m_intrin_color.width + cx) * 3);
                m_mutexs_ref->unlock();
            }

            // Emit a vertex at the 3D location of this depth pixel
            glVertex3f(depth_point.x, depth_point.y, depth_point.z);
        }
    }

    glEnd();
}
void RealsenseCamera::display_point_cloud()
{
    set_perspective_transform();
    render_3D_depth_data();
}

void RealsenseCamera::display_rgbd_stream()
{
    // Wait for new frame data
    glClear(GL_COLOR_BUFFER_BIT);
    glPixelZoom(1, -1);

    // Display depth data by linearly mapping depth between 0 and 2 meters to the red channel
    glRasterPos2f(-1, 1);
    glPixelTransferf(GL_RED_SCALE, 0xFFFF * m_camera->m_depth_scale / 2.0f);
    m_mutexs_ref->lock();
    glDrawPixels(640, 480, GL_RED, GL_UNSIGNED_SHORT, m_camera->m_depth_image);
    m_mutexs_ref->unlock();
    glPixelTransferf(GL_RED_SCALE, 1.0f);

    // Display color image as RGB triples
    glRasterPos2f(0, 1);
    m_mutexs_ref->lock();
    glDrawPixels(640, 480, GL_RGB, GL_UNSIGNED_BYTE, m_camera->m_color_image);
    m_mutexs_ref->unlock();
    // Display infrared image by mapping IR intensity to visible luminance
    glRasterPos2f(-1, 0);
    m_mutexs_ref->lock();
    glDrawPixels(640, 480, GL_LUMINANCE, GL_UNSIGNED_BYTE, m_camera->m_infrared_image);
    m_mutexs_ref->unlock();

    // Display second infrared image by mapping IR intensity to visible luminance
    if(m_camera->m_dev->is_stream_enabled(rs::stream::infrared2))
    {
        glRasterPos2f(0, 0);
        m_mutexs_ref->lock();
        glDrawPixels(640, 480, GL_LUMINANCE, GL_UNSIGNED_BYTE, m_camera->m_infrared2_image);
        m_mutexs_ref->unlock();
    }
}

void RealsenseCamera::display_frame()
{
    glfw_init();
    
    while(!glfwWindowShouldClose(m_win))
    {
        glfwPollEvents();
        display_rgbd_stream();
        //display_point_cloud();
        glfwSwapBuffers(m_win);
    }
}
