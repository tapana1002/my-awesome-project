/*************************************************************************
	> File Name: realsenseCamera.cpp
	> Author: 
	> Mail: 
	> Created Time: Mon 22 May 2017 05:13:20 PM PDT
 ************************************************************************/

#include "RealsenseManager.h"
#include "Common.h"

extern COMMAND_LINE_ARGS g_command_line_args;

RealsenseManager::RealsenseManager()
{

}

RealsenseManager::~RealsenseManager()
{
    for(int i=0; i<m_device_num; i++)
        delete(m_rs_camera[i]);

    delete m_ctx;
}

/**
 * @name    get_realsense_camera_count
 * @brief   Returns the number of RealSense cameras
 * @ingroup Realsense Manager
 * @retval  INT Number of detected realsense cameras
 */
int  get_realsense_camera_count()
{
    rs::context* tmp_ctx;
    u08 device_num;
    tmp_ctx = new(rs::context);
    
    LOG(INFO) << "Looking for RealSense Cameras...";
    
    device_num = tmp_ctx->get_device_count();
    free(tmp_ctx);
    return device_num;
}

int RealsenseManager::start_realsense()
{
    /**
    * Create a context object. This object owns the handles to all connected realsense devices.
    */
    m_ctx = new(rs::context);

    /**
    *   Get the number of devices
    */
    m_device_num = m_ctx->get_device_count();

    if(m_ctx->get_device_count() == 0) 
        return false;

    LOG(INFO) << "There are " << std::to_string(m_device_num) << " connected RealSense devices.\n" << std::endl;

    /**
    *   Access multiple devices
    */ 
    for(int i=0; i<m_device_num; i++)
    {
        m_camera[i].m_dev = m_ctx->get_device(i);
        m_camera[i].m_camera_id = i;
        
        /**
        *   Show the device name and information
        */
        m_camera[i].m_name = m_camera[i].m_dev->get_name();
        LOG(INFO)  << "\tDevice " << i << " - " << m_camera[i].m_name << ":\n";

        m_camera[i].m_serial = m_camera[i].m_dev->get_serial();
        std::cout << "\tSerial number: " << m_camera[i].m_serial << "\n";
        
        try 
        { 
            m_camera[i].m_usb_port_id = m_camera[i].m_dev->get_usb_port_id();
            std::cout << "\tUSB Port ID: " << m_camera[i].m_usb_port_id << "\n";
        } catch (...) {}

        m_camera[i].m_firmware_version = m_camera[i].m_dev->get_firmware_version();
        std::cout << "\tFirmware version: " << m_camera[i].m_dev->get_firmware_version() << "\n";

        if (m_camera[i].m_dev->supports(rs::capabilities::adapter_board)) 
            std::cout << " Adapter Board Firmware version: " << m_camera[i].m_dev->get_info(rs::camera_info::adapter_board_firmware_version) << "\n";
        if (m_camera[i].m_dev->supports(rs::capabilities::motion_events)) 
            std::cout << " Motion Module Firmware version: " << m_camera[i].m_dev->get_info(rs::camera_info::motion_module_firmware_version) << "\n";
    }

    for(int i=0; i<m_device_num; i++)
    {
        #if 0   // point cloud
        /**
        *   Configure depth and color to run with the device's preferred settings
        */
        m_camera[i].m_dev->enable_stream(rs::stream::depth, rs::preset::best_quality);
        m_camera[i].m_dev->enable_stream(rs::stream::color, rs::preset::best_quality);
        m_camera[i].m_dev->start();

        #else // streaming   
        // Configure all streams to run at VGA resolution at 60 frames per second
        m_camera[i].m_dev->enable_stream(rs::stream::depth, 640, 480, rs::format::z16, 60);
        if(g_command_line_args.rgb_save_jpeg_enabled || g_command_line_args.rgb_save_avi_enabled || g_command_line_args.zbar_enabled)
        {
            m_camera[i].m_dev->enable_stream(rs::stream::color, 1920, 1080, rs::format::rgb8, 30);
        } else
        {
            m_camera[i].m_dev->enable_stream(rs::stream::color, 640, 480, rs::format::rgb8, 60);
        }
        m_camera[i].m_dev->enable_stream(rs::stream::infrared, 640, 480, rs::format::y8, 60);
        try { m_camera[i].m_dev->enable_stream(rs::stream::infrared2, 640, 480, rs::format::y8, 60); }
        catch(...) { printf("Device does not provide infrared2 stream.\n"); }
        m_camera[i].m_dev->start();
        #endif 

        /**
        *   Retrieve camera parameters for mapping between depth and color
        */
        m_camera[i].m_intrin_depth = m_camera[i].m_dev->get_stream_intrinsics(rs::stream::depth);
        m_camera[i].m_intrin_color = m_camera[i].m_dev->get_stream_intrinsics(rs::stream::color);
        m_camera[i].m_extrin = m_camera[i].m_dev->get_extrinsics(rs::stream::depth, rs::stream::color);
        m_camera[i].m_depth_scale = m_camera[i].m_dev->get_depth_scale();
    }

    return true;
}

/**
*   Read the latest frame from each device, one thread for one device
*/
void RealsenseManager::read_frame_thread(int m_camera_id)
{
    while(1)
    {
        m_camera[m_camera_id].m_dev->wait_for_frames();
        m_mutexs[m_camera_id].lock();
        //m_camera[m_camera_id].dev->get_stream_mode(strm, k, width, height, format, framerate);
        m_camera[m_camera_id].m_frame_number = m_camera[m_camera_id].m_dev->get_frame_number(rs::stream::depth);
        m_camera[m_camera_id].m_time_stamp = m_camera[m_camera_id].m_dev->get_frame_timestamp(rs::stream::depth);
        m_camera[m_camera_id].m_stream_rate = m_camera[m_camera_id].m_dev->get_stream_framerate(rs::stream::depth);
        m_camera[m_camera_id].m_stream_height = m_camera[m_camera_id].m_dev->get_stream_height(rs::stream::depth);
        m_camera[m_camera_id].m_stream_width = m_camera[m_camera_id].m_dev->get_stream_width(rs::stream::depth);
        m_camera[m_camera_id].m_depth_image = (const u16 *)m_camera[m_camera_id].m_dev->get_frame_data(rs::stream::depth);
        m_camera[m_camera_id].m_color_image = (const u08 *)m_camera[m_camera_id].m_dev->get_frame_data(rs::stream::color);
        m_camera[m_camera_id].m_infrared_image = (const u08 *)m_camera[m_camera_id].m_dev->get_frame_data(rs::stream::infrared);
        m_camera[m_camera_id].m_infrared2_image = (const u08 *)m_camera[m_camera_id].m_dev->get_frame_data(rs::stream::infrared2);
        m_camera[m_camera_id].m_point_cloud = (const rs::float3 *)m_camera[m_camera_id].m_dev->get_frame_data(rs::stream::points);
        //std::cout << "frame-number-" << m_camera[m_camera_id].m_frame_number << "-time-stamp-" << m_camera[m_camera_id].m_time_stamp <<"\n";
        m_mutexs[m_camera_id].unlock();
    }
}


/**
*   Spawn the read frame threads for all the devices
*/
void RealsenseManager::start_read_frame_threads()
{
    for(int camera_id=0; camera_id<m_device_num; camera_id++)
    {   
        m_driver_thread_id[camera_id] = std::thread(&RealsenseManager::read_frame_thread, this, camera_id);
        m_driver_thread_id[camera_id].detach();
    }
}

void RealsenseManager::start_camera_loop_threads()
{
    for(int camera_id=0; camera_id<m_device_num; camera_id++)
    {
        m_rs_camera[camera_id] = new RealsenseCamera(&m_camera[camera_id], &m_mutexs[camera_id]); 
        m_app_thread_id[camera_id] = std::thread(&RealsenseCamera::camera_loop, m_rs_camera[camera_id]);
        m_app_thread_id[camera_id].detach();
    }
}

void RealsenseManager::realsense_manager_loop()
{
    start_realsense();
    start_read_frame_threads();
    start_camera_loop_threads();
}


