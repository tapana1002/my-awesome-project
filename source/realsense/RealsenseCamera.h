/*************************************************************************
	> File Name: realsenseCamera.h
	> Author: 
	> Mail: 
	> Created Time: Mon 22 May 2017 05:15:00 PM PDT
 ************************************************************************/

#ifndef _REALSENSECAMERA_H
#define _REALSENSECAMERA_H

#include "Common.h"
#include <librealsense/rs.hpp>
#include <cstdio>
#define GLFW_INCLUDE_GLU
#include <GLFW/glfw3.h>
#include <GL/glut.h>
#include <jsoncpp/json/json.h>
#include <jsoncpp/json/writer.h>



/**
*	An realsense R200 camera device parameters
*/
typedef struct
{
	/**
	*	realsense camera device object
	*/
	rs::device * m_dev;

	/**
	*	realsense camera device information
	*/
	const char* m_name;
	const char* m_serial;
	int m_camera_id;
	const char* m_usb_port_id;
	const char* m_firmware_version;
	char* m_camera_info;
	rs::extrinsics m_extrin;
	rs::intrinsics m_intrin_color;
	rs::intrinsics m_intrin_depth;
	float m_depth_scale;
	
	/**
	*	current frame information
	*/
	unsigned long long m_frame_number;
	int m_stream_rate;
	int m_stream_height;
	int m_stream_width;
	double m_time_stamp;
	const u16* m_depth_image;
	const u08* m_color_image;
	const u08* m_infrared_image;
	const u08* m_infrared2_image;
	const rs::float3* m_point_cloud;
}CameraInfo;


/**
*	An realsense R200 camera device parameters
*/
class RealsenseCamera{
	public:
		RealsenseCamera(CameraInfo* camera, std::mutex* mtx); 
		virtual ~RealsenseCamera();

		/**
		*	save frame to different formats
		*/
		void save_depth_frame_csv();
		void save_point_cloud_csv();
		void save_point_cloud_color_csv();
		void save_depth_frame_json();
		void save_point_cloud_json();
		void save_point_cloud_color_json();
		void save_point_cloud_ply();
		void save_point_cloud_color_ply();

		void save_depth_image();
		void save_rgb_image();
		void save_ir_image();
		void save_rgb_video();
		void zbar_scan_color();

		void record_frame();
		/**
		*	camera loop thread for processing frames
		*/
		
    	void camera_loop();

    	/**
    	*	log config for realsense libraries, once enabled the debugging/log info will be output to a file
    	*/
    	void log_config();

    	/**
    	*	opengl initialization for display
    	*/
		int	glfw_init(); 
		void display_frame(); 	
		/**
		*	display member function
		*/
		void set_perspective_transform();
		void render_3D_depth_data();

		/**
		*	streaming frames
		*/
		void display_rgbd_stream();

		/**
		*	point clouds
		*/
		void display_point_cloud();
		
		/**
		*	contains camera parameters and the latest frame from device
		*/
		CameraInfo* m_camera;
		
		std::thread m_camera_thread;
		bool 		m_zbar_enabled;
		const int   m_depth_image_size = 640 * 480;
	private:
		
		
		std::mutex* m_mutexs_ref;


		/**
		*	opengl windows handle
		*/
		GLFWwindow * m_win;
};

 

#endif 
