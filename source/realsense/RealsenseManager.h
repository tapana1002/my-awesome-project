/*************************************************************************
	> File Name: realsenseCamera.h
	> Author: 
	> Mail: 
	> Created Time: Mon 22 May 2017 05:15:00 PM PDT
 ************************************************************************/

#ifndef _REALSENSEMANAGER_H
#define _REALSENSEMANAGER_H

#include "RealsenseCamera.h"
#include "Common.h"
#include <librealsense/rs.hpp>
#include <cstdio>
#define GLFW_INCLUDE_GLU
#include <GLFW/glfw3.h>
#include <GL/glut.h>


/**
*	Realsense Manager
*	Initialize camera, get frames from each camera and store frame into each camera frame buffer
*/
class RealsenseManager{
	public:
		RealsenseManager();
		virtual ~RealsenseManager();
		void realsense_manager_loop();

		CameraInfo m_camera[MAX_CAMERA_NUM];
		std::thread m_driver_thread_id[MAX_CAMERA_NUM];

		RealsenseCamera* m_rs_camera[MAX_CAMERA_NUM];
		std::thread m_app_thread_id[MAX_CAMERA_NUM];


	private:
		u08 m_device_num;
		rs::context* m_ctx;
		std::mutex m_mutexs[MAX_CAMERA_NUM];

		int start_realsense();
		void start_read_frame_threads();
		void read_frame_thread(int m_camera_id);
		void start_camera_loop_threads();
};

/*
 * \Descr temporarily initialize realsense lib to get camera count
 * \param
 */
int  get_realsense_camera_count();

#endif 
