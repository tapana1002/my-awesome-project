#include <opencv2/opencv.hpp>
#include <iostream>
#include <librealsense/rs.hpp>

// Only for Intel r200 cameras 
namespace mashgin
{
	using std::vector;
	using std::string;
	using cv::Mat;
	using cv::Point3f;
	using cv::Vec3b;

	struct Pixel3D_t
	{
		Point3f position;
		Vec3b color;
		Pixel3D_t(Point3f pos, Vec3b clr): position(pos), color(clr) {};
		Pixel3D_t() {};
	};

	// 'camid' can be any unique identifier, like the camera serial nr
	void SetParams(string camid, rs::intrinsics depth_intrin, rs::extrinsics depth_to_color, rs::intrinsics color_intrin, float scale);
	void SetBoundingBox(float minx, float maxx, float miny, float maxy, float minz, float maxz);

	bool Calibrate(string camid, Mat image, Mat depth, bool plot=true);
	bool SaveCalibration(string camid, string folder_path);
	bool LoadCalibration(string camid, string folder_path);

	vector<Pixel3D_t> GetPointCloud(string camid, Mat image, Mat depth);
	vector<Pixel3D_t> GetWorldPointCloud(string camid, vector<Pixel3D_t>& cloud);
	bool SaveToPly(string filename, vector<Pixel3D_t>& cloud);

	Mat GetProjection(vector<Pixel3D_t>& world_cloud, string type ="z");
}