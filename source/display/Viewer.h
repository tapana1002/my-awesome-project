/*****************************************************************************
*                                                                            *
*  OpenNI 2.x Alpha                                                          *
*  Copyright (C) 2012 PrimeSense Ltd.                                        *
*                                                                            *
*  This file is part of OpenNI.                                              *
*                                                                            *
*  Licensed under the Apache License, Version 2.0 (the "License");           *
*  you may not use this file except in compliance with the License.          *
*  You may obtain a copy of the License at                                   *
*                                                                            *
*      http://www.apache.org/licenses/LICENSE-2.0                            *
*                                                                            *
*  Unless required by applicable law or agreed to in writing, software       *
*  distributed under the License is distributed on an "AS IS" BASIS,         *
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *
*  See the License for the specific language governing permissions and       *
*  limitations under the License.                                            *
*                                                                            *
*****************************************************************************/
#ifndef _VIEWER_H_
#define _VIEWER_H_

#include <OpenNI.h>

#include "OrbbecCamera.h"
#include "pngwriter.h"

/* Bring in standard I/O so we can output the PNG to a file */
#include <stdio.h>

#define MAX_DEPTH 10000

typedef enum DisplayModes
{
	DISPLAY_MODE_OVERLAY,
	DISPLAY_MODE_DEPTH,
	DISPLAY_MODE_IMAGE
}DisplayModes;

class Viewer
{
friend class OrbbecCamera;

public:
	Viewer(const char* strSampleName, openni::Device& device, openni::VideoStream& depth, openni::VideoStream& color, std::recursive_mutex& frame_mutex, openni::VideoFrameRef& depth_frame, openni::VideoFrameRef& color_frame);
	virtual ~Viewer();

	virtual openni::Status init(int argc, char **argv);
	virtual openni::Status run();	//Does not return

	virtual void lock_frame_mutex();
	virtual void unlock_frame_mutex();

	virtual void save_frame_2_photo();

protected:
	virtual void display();
	virtual void displayPostDraw(){};	// Overload to draw over the screen image
	virtual void onKey(unsigned char key, int x, int y);
	virtual openni::Status initOpenGL(int argc, char **argv);
	void initOpenGLHooks();

private:
	Viewer(const Viewer&);
	Viewer& operator=(Viewer&);

	static Viewer* ms_self;
	static void glutIdle();
	static void glutDisplay();
	static void glutKeyboard(unsigned char key, int x, int y);

	openni::Device&         m_device_ref;
	openni::VideoStream&    m_depth_stream_ref;
	openni::VideoStream&    m_color_stream_ref;
	std::recursive_mutex&   m_frame_mutex_ref;
	openni::VideoFrameRef&  m_current_depth_frame;
    openni::VideoFrameRef&  m_current_color_frame;


	float				 m_pDepthHist[MAX_DEPTH];
	char				 m_strSampleName[ONI_MAX_STR];
	unsigned int		 m_nTexMapX;
	unsigned int		 m_nTexMapY;
	DisplayModes		 m_eViewState;
	openni::RGB888Pixel* m_pTexMap;
	int					 m_width;
	int				     m_height;
	bool				 m_save_frame_flag;
};


#endif // VIEWER_H

