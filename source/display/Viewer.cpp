/*****************************************************************************
*                                                                            *
*  OpenNI 2.x Alpha                                                          *
*  Copyright (C) 2012 PrimeSense Ltd.                                        *
*                                                                            *
*  This file is part of OpenNI.                                              *
*                                                                            *
*  Licensed under the Apache License, Version 2.0 (the "License");           *
*  you may not use this file except in compliance with the License.          *
*  You may obtain a copy of the License at                                   *
*                                                                            *
*      http://www.apache.org/licenses/LICENSE-2.0                            *
*                                                                            *
*  Unless required by applicable law or agreed to in writing, software       *
*  distributed under the License is distributed on an "AS IS" BASIS,         *
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *
*  See the License for the specific language governing permissions and       *
*  limitations under the License.                                            *
*                                                                            *
*****************************************************************************/
// Undeprecate CRT functions
#ifndef _CRT_SECURE_NO_DEPRECATE 
	#define _CRT_SECURE_NO_DEPRECATE 1
#endif

#include "Viewer.h"
#include "Common.h"

#if (ONI_PLATFORM == ONI_PLATFORM_MACOSX)
        #include <GLUT/glut.h>
#else
        #include <GL/glut.h>
#endif

#include "OniSampleUtilities.h"

#define GL_WIN_SIZE_X	1280
#define GL_WIN_SIZE_Y	1024
#define TEXTURE_SIZE	512

#define DEFAULT_DISPLAY_MODE	DISPLAY_MODE_DEPTH

#define MIN_NUM_CHUNKS(data_size, chunk_size)	((((data_size)-1) / (chunk_size) + 1))
#define MIN_CHUNKS_SIZE(data_size, chunk_size)	(MIN_NUM_CHUNKS(data_size, chunk_size) * (chunk_size))
using namespace std;

Viewer* Viewer::ms_self = NULL;

void Viewer::glutIdle()
{
	glutPostRedisplay();
}
void Viewer::glutDisplay()
{
	//Viewer::ms_self->lock_frame_mutex();
	Viewer::ms_self->display();
	//Viewer::ms_self->unlock_frame_mutex();
}
void Viewer::glutKeyboard(unsigned char key, int x, int y)
{
	Viewer::ms_self->onKey(key, x, y);
}

void Viewer::lock_frame_mutex()
{
	m_frame_mutex_ref.lock();
}

void Viewer::unlock_frame_mutex()
{
	m_frame_mutex_ref.unlock();
}

Viewer::Viewer(const char* strSampleName, openni::Device& device, openni::VideoStream& depth, openni::VideoStream& color, std::recursive_mutex& frame_mutex, openni::VideoFrameRef &depth_frame, openni::VideoFrameRef &color_frame) :
 	m_device_ref(device), m_depth_stream_ref(depth), m_color_stream_ref(color), m_frame_mutex_ref(frame_mutex), m_current_depth_frame(depth_frame), m_current_color_frame(color_frame), m_eViewState(DEFAULT_DISPLAY_MODE), m_pTexMap(NULL)

{
	ms_self = this;
	strncpy(m_strSampleName, strSampleName, ONI_MAX_STR);
}
Viewer::~Viewer()
{
	delete[] m_pTexMap;

	ms_self = NULL;

}

openni::Status Viewer::init(int argc, char **argv)
{
	openni::VideoMode depthVideoMode;
	openni::VideoMode colorVideoMode;

	if ( m_depth_stream_ref.isValid() && m_color_stream_ref.isValid())
	{
		depthVideoMode = m_depth_stream_ref.getVideoMode();
		colorVideoMode = m_color_stream_ref.getVideoMode();

		int depthWidth = depthVideoMode.getResolutionX();
		int depthHeight = depthVideoMode.getResolutionY();
		int colorWidth = colorVideoMode.getResolutionX();
		int colorHeight = colorVideoMode.getResolutionY();

		if (depthWidth == colorWidth &&
			depthHeight == colorHeight)
		{
			m_width = depthWidth;
			m_height = depthHeight;
		}
		else
		{
			printf("Error - expect color and depth to be in same resolution: D: %dx%d, C: %dx%d\n",
				depthWidth, depthHeight,
				colorWidth, colorHeight);
			return openni::STATUS_ERROR;
		}
	}
	else if (m_depth_stream_ref.isValid())
	{
		depthVideoMode = m_depth_stream_ref.getVideoMode();
		m_width = depthVideoMode.getResolutionX();
		m_height = depthVideoMode.getResolutionY();
	}
	else if (m_color_stream_ref.isValid())
	{
		colorVideoMode = m_color_stream_ref.getVideoMode();
		m_width = colorVideoMode.getResolutionX();
		m_height = colorVideoMode.getResolutionY();
	}
	else
	{
		printf("Error - expects at least one of the streams to be valid...\n");
		return openni::STATUS_ERROR;
	}

	// Texture map init
	m_nTexMapX = MIN_CHUNKS_SIZE(m_width, TEXTURE_SIZE);
	m_nTexMapY = MIN_CHUNKS_SIZE(m_height, TEXTURE_SIZE);
	m_pTexMap = new openni::RGB888Pixel[m_nTexMapX * m_nTexMapY];

	m_save_frame_flag = false;

	return initOpenGL(argc, argv);

}

openni::Status Viewer::run()	//Does not return
{
	glutMainLoop();

	return openni::STATUS_OK;
}

void Viewer::display()
{
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0, GL_WIN_SIZE_X, GL_WIN_SIZE_Y, 0, -1.0, 1.0);

	if (m_current_depth_frame.isValid())
	{
		calculateHistogram(m_pDepthHist, MAX_DEPTH, m_current_depth_frame);
	}

	memset(m_pTexMap, 0, m_nTexMapX*m_nTexMapY*sizeof(openni::RGB888Pixel));

	// check if we need to draw color image frame to texture
	if ((m_eViewState == DISPLAY_MODE_OVERLAY ||
		m_eViewState == DISPLAY_MODE_IMAGE) && m_current_color_frame.isValid())
	{
		Viewer::ms_self->lock_frame_mutex();
		const openni::RGB888Pixel* pImageRow = (const openni::RGB888Pixel*)m_current_color_frame.getData();
		Viewer::ms_self->unlock_frame_mutex();
		openni::RGB888Pixel* pTexRow = m_pTexMap + m_current_color_frame.getCropOriginY() * m_nTexMapX;
		int rowSize = m_current_color_frame.getStrideInBytes() / sizeof(openni::RGB888Pixel);

		for (int y = 0; y < m_current_color_frame.getHeight(); ++y)
		{
			const openni::RGB888Pixel* pImage = pImageRow;
			openni::RGB888Pixel* pTex = pTexRow + m_current_color_frame.getCropOriginX();

			for (int x = 0; x < m_current_color_frame.getWidth(); ++x, ++pImage, ++pTex)
			{
				*pTex = *pImage;
			}

			pImageRow += rowSize;
			pTexRow += m_nTexMapX;
		}
	}

	// check if we need to draw depth frame to texture
	if ((m_eViewState == DISPLAY_MODE_OVERLAY ||
		m_eViewState == DISPLAY_MODE_DEPTH) && m_current_depth_frame.isValid())
	{
		Viewer::ms_self->lock_frame_mutex();
		const openni::DepthPixel* pDepthRow = (const openni::DepthPixel*)m_current_depth_frame.getData();
		Viewer::ms_self->unlock_frame_mutex();
		openni::RGB888Pixel* pTexRow = m_pTexMap + m_current_depth_frame.getCropOriginY() * m_nTexMapX;
		int rowSize = m_current_depth_frame.getStrideInBytes() / sizeof(openni::DepthPixel);

		for (int y = 0; y < m_current_depth_frame.getHeight(); ++y)
		{
			const openni::DepthPixel* pDepth = pDepthRow;
			openni::RGB888Pixel* pTex = pTexRow + m_current_depth_frame.getCropOriginX();

			for (int x = 0; x < m_current_depth_frame.getWidth(); ++x, ++pDepth, ++pTex)
			{
				if (*pDepth != 0)
				{
					int nHistValue = m_pDepthHist[*pDepth];
					pTex->r = nHistValue;
					pTex->g = nHistValue;
					pTex->b = 0;
				}
			}

			pDepthRow += rowSize;
			pTexRow += m_nTexMapX;
		}
	}

	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP_SGIS, GL_TRUE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_nTexMapX, m_nTexMapY, 0, GL_RGB, GL_UNSIGNED_BYTE, m_pTexMap);

	// Display the OpenGL texture map
	glColor4f(1,1,1,1);


	glBegin(GL_QUADS);

	int nXRes = m_width;
	int nYRes = m_height;

	// upper left
	glTexCoord2f(0, 0);
	glVertex2f(0, 0);
	// upper right
	glTexCoord2f((float)nXRes/(float)m_nTexMapX, 0);
	glVertex2f(GL_WIN_SIZE_X, 0);
	// bottom right
	glTexCoord2f((float)nXRes/(float)m_nTexMapX, (float)nYRes/(float)m_nTexMapY);
	glVertex2f(GL_WIN_SIZE_X, GL_WIN_SIZE_Y);
	// bottom left
	glTexCoord2f(0, (float)nYRes/(float)m_nTexMapY);
	glVertex2f(0, GL_WIN_SIZE_Y);

	glEnd();

	// Swap the OpenGL display buffers
	glutSwapBuffers();

}

void Viewer::save_frame_2_photo()
{
	cv::Mat colorImage;
	openni::RGB888Pixel* pColor;

	static int photo_cnts = 0;

    vector<int> compression_params; //vector that stores the compression parameters of the image
    compression_params.push_back(CV_IMWRITE_JPEG_QUALITY); //specify the compression technique
    compression_params.push_back(9); //specify the compression quality

	if(m_current_color_frame.isValid())
	{
		string image_name;
		image_name = "image" + to_string(photo_cnts) +".jpeg";

		cout << "save_frame_2_photo...\n";
		colorImage.create(480, 640, CV_8UC3);
		pColor = (openni::RGB888Pixel*)m_current_color_frame.getData();
		int step24 = colorImage.step;
		memcpy(colorImage.data, pColor, step24*480);
		cv::imwrite(image_name, colorImage, compression_params); //write the image to file
	}

	photo_cnts++;
}

void Viewer::onKey(unsigned char key, int /*x*/, int /*y*/)
{
	switch (key)
	{
	case 27:
		m_depth_stream_ref.stop();
		m_color_stream_ref.stop();
		m_depth_stream_ref.destroy();
		m_color_stream_ref.destroy();
		m_device_ref.close();
		openni::OpenNI::shutdown();

		exit (1);
	case '1':
		m_eViewState = DISPLAY_MODE_OVERLAY;
		m_device_ref.setImageRegistrationMode(openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR);
		break;

	case '2':
		m_eViewState = DISPLAY_MODE_DEPTH;
		m_device_ref.setImageRegistrationMode(openni::IMAGE_REGISTRATION_OFF);
		break;

	case '3':
		m_eViewState = DISPLAY_MODE_IMAGE;
		m_device_ref.setImageRegistrationMode(openni::IMAGE_REGISTRATION_OFF);
		break;

	case '4':
		save_frame_2_photo();

	case 'm':
		m_depth_stream_ref.setMirroringEnabled(!m_depth_stream_ref.getMirroringEnabled());
		m_color_stream_ref.setMirroringEnabled(!m_color_stream_ref.getMirroringEnabled());
		break;
	case 'c':
		m_save_frame_flag = true;
	}
}

openni::Status Viewer::initOpenGL(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(GL_WIN_SIZE_X, GL_WIN_SIZE_Y);
	glutCreateWindow (m_strSampleName);
	glutSetCursor(GLUT_CURSOR_NONE);

	initOpenGLHooks();

	glDisable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);

	return openni::STATUS_OK;

}
void Viewer::initOpenGLHooks()
{
	glutKeyboardFunc(glutKeyboard);
	glutDisplayFunc(glutDisplay);
	glutIdleFunc(glutIdle);
}
