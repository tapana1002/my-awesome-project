##===============================================================================
##		Application source directories 	
##===============================================================================
set(COMMON_SRC_DIR 		${CMAKE_SOURCE_DIR}/source/common)
set(APP_SRC_DIR 		${CMAKE_SOURCE_DIR}/source/application)
set(COROS_SRC_DIR 		${CMAKE_SOURCE_DIR}/source/coros)
set(ORBBEC_SRC_DIR 		${CMAKE_SOURCE_DIR}/source/orbbec)
set(REALSENSE_SRC_DIR 	${CMAKE_SOURCE_DIR}/source/realsense)
set(DISP_SRC_DIR 		${CMAKE_SOURCE_DIR}/source/display)
set(OPENNI_SRC_DIR 		${CMAKE_SOURCE_DIR}/source/openni)
set(EASYLOG_SRC_DIR 	${CMAKE_SOURCE_DIR}/source/easyloggingpp)
set(RECORD_SRC_DIR 		${CMAKE_SOURCE_DIR}/source/record)
set(STREAM_SRC_DIR 		${CMAKE_SOURCE_DIR}/source/streaming)
set(PROTO_SRC_DIR 		${CMAKE_SOURCE_DIR}/source/streaming/proto)
set(OPENCV_SRC_DIR 		${CMAKE_SOURCE_DIR}/source/opencv_example)
set(IMGPROC_SRC_DIR 	${CMAKE_SOURCE_DIR}/source/image_processing)
set(3DLIB_SRC_DIR 		${CMAKE_SOURCE_DIR}/source/m_3d_library/)

##===============================================================================
##		Application source file path		
##===============================================================================
set(COM_SRC 		${COMMON_SRC_DIR}/usb_util.cpp)
set(APP_SRC 		${APP_SRC_DIR}/main.cpp)
set(COROS_SRC 		${COROS_SRC_DIR}/CoROS.cpp
					${COROS_SRC_DIR}/InventoryManager.cpp)
set(ORBBEC_SRC 		${ORBBEC_SRC_DIR}/OrbbecCamera.cpp
					${ORBBEC_SRC_DIR}/OrbbecManager.cpp 
					${ORBBEC_SRC_DIR}/COBDevice.cpp)
set(REALSENSE_SRC 	${REALSENSE_SRC_DIR}/RealsenseCamera.cpp
					${REALSENSE_SRC_DIR}/RealsenseManager.cpp)
set(DISP_SRC		${DISP_SRC_DIR}/Viewer.cpp)
set(RECORD_SRC		${RECORD_SRC_DIR}/record.cpp)
set(LOG_SRC			${EASYLOG_SRC_DIR}/easylogging++.cc)
set(STREAM_SRC		${STREAM_SRC_DIR}/stream_server.cpp 
					${STREAM_SRC_DIR}/stream_client.cpp)
set(PROTO_SRC		${STREAM_SRC_DIR}/proto/coros_stream.pb.cc
					${STREAM_SRC_DIR}/proto/coros_stream.grpc.pb.cc)
set(OPENCV_SRC		${OPENCV_SRC_DIR}/opencv_example.cpp)
set(IMAGEPROC_SRC   ${IMGPROC_SRC_DIR}/ImageProcessing.cpp)

set(ALL_SRC
	${COM_SRC}
	${APP_SRC}
	${COROS_SRC}
	${ORBBEC_SRC}
	${REALSENSE_SRC}
	${DISP_SRC}
	${RECORD_SRC}
	${LOG_SRC}
	${STREAM_SRC}
	${PROTO_SRC}
	${OPENCV_SRC}
	${IMAGEPROC_SRC}
	)
	
	
##================================================================================
##		Application incude directories 	
##================================================================================
include_directories(${COMMON_SRC_DIR})
include_directories(${APP_SRC_DIR})
include_directories(${COROS_SRC_DIR})
include_directories(${ORBBEC_SRC_DIR})
include_directories(${REALSENSE_SRC_DIR})
include_directories(${DISP_SRC_DIR})
include_directories(${RECORD_SRC_DIR})
include_directories(${EASYLOG_SRC_DIR})
include_directories(${STREAM_SRC_DIR})
include_directories(${PROTO_SRC_DIR})
include_directories(${OPENCV_SRC_DIR})
include_directories(${IMGPROC_SRC_DIR})
include_directories(${3DLIB_SRC_DIR})

##================================================================================
##		Set and Build Target  		 	
##================================================================================
set(TARGET_NAME CoROS)

add_executable(${TARGET_NAME} ${ALL_SRC})
target_link_libraries(${TARGET_NAME} ${LIBRARIES})

# Build a library from all specified source files
# This is required for using googletest
add_library(${TARGET_NAME}-lib ${ALL_SRC})
target_link_libraries(${TARGET_NAME}-lib ${LIBRARIES})
set_target_properties(${TARGET_NAME}-lib PROPERTIES OUTPUT_NAME CoROS)

# vim:ft=cmake


