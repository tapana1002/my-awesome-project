/* ========================================
*
* Copyright Daimler, 2017
* All Rights Reserved
* UNPUBLISHED, LICENSED SOFTWARE.
*
* CONFIDENTIAL AND PROPRIETARY INFORMATION
* WHICH IS THE PROPERTY OF Daimler.
*
* ========================================
*/
#include "CoROS.h"
using namespace std;

COMMAND_LINE_ARGS g_command_line_args;

/* Initialize system logging*/
INITIALIZE_EASYLOGGINGPP 

/*
 * \Descr main function: Construct CoROS oject and start camera_loop threads.
 * \param
 */
int main(int argc, char** argv)
{
#if OPENCV_EXAMPLE
	#include "opencv_example.h"
	opencv_main();
	return 0;
#else
	
	/* Register CTRL-C with a function call */
	signal(SIGINT, CoROS::coros_shutdown); 

	/*Setup Easyloggingpp library.*/
    el::Configurations conf("../log-conf.config");
    el::Loggers::reconfigureLogger("default", conf);
    el::Loggers::reconfigureAllLoggers(conf)
	START_EASYLOGGINGPP(argc, argv);
	
	/*Print App prompt.*/
	cout << coros_str << endl;
	cout << "       v1.0.0\n" << endl; //should add this to a macro
	LOG(INFO) << "Starting Application.";
	
	/*Parse cmdline arguments for coros app*/
	g_command_line_args.argc = argc;
	g_command_line_args.argv = argv;
	CoROS::cmd_parser(g_command_line_args.argc, g_command_line_args.argv);

	//All the magic begins in this constructor
	CoROS coros;

	while(1)
	{
		/*
		* main thread keeps running
		* play the same role as join()
		* TODO: add application state management code here
		*/
	}
#endif
}

