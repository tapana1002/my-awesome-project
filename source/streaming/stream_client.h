/*************************************************************************
	> File Name: streaming.h
	> Author: 
	> Mail: 
	> Created Time: Wed 07 Jun 2017 03:36:54 PM PDT
 ************************************************************************/

#ifndef _STREAMING_CLIENT_H
#define _STREAMING_CLIENT_H

#include <grpc/grpc.h>
#include <grpc++/channel.h>
#include <grpc++/client_context.h>
#include <grpc++/create_channel.h>
#include <grpc++/security/credentials.h>
#include "coros_stream.grpc.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;
using grpc::Status;

using corosstream::CorosStreamService;
using corosstream::StreamData;
using corosstream::StreamInfo;
using corosstream::StreamRequest;
using corosstream::QRCode;
using corosstream::ReadQRCodeResponse;

class CorosStreamClientImpl 
{
	public:
		CorosStreamClientImpl(std::shared_ptr<Channel> channel):stub_(CorosStreamService::NewStub(channel)) {}
		void CorosStream();
		void ReadQRCode();

	private:
		std::unique_ptr<CorosStreamService::Stub> stub_;
};

class CorosStreamClient 
{
	public:
		CorosStreamClient(){RunClient();}
	    void RunClient();

	private:

};


#endif
