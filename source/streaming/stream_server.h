/*************************************************************************
	> File Name: streaming.h
	> Author: 
	> Mail: 
	> Created Time: Wed 07 Jun 2017 03:36:54 PM PDT
 ************************************************************************/

#ifndef _STREAMING_SERVER_H
#define _STREAMING_SERVER_H

#include <grpc/grpc.h>
#include <grpc++/server.h>
#include <grpc++/server_builder.h>
#include <grpc++/server_context.h>
#include <grpc++/security/server_credentials.h>
#include "coros_stream.grpc.pb.h"

using std::chrono::system_clock;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerReaderWriter;
using grpc::ServerWriter;
using grpc::Status;
using corosstream::CorosStreamService;
using corosstream::StreamData;
using corosstream::StreamInfo;
using corosstream::StreamRequest;
using corosstream::QRCode;
using corosstream::ReadQRCodeResponse;


class CorosStreamServerImpl final : public CorosStreamService::Service 
{
public:
	explicit CorosStreamServerImpl(){};
	Status CorosStream(ServerContext* context,const StreamRequest* camera,ServerWriter<StreamData>* writer) override;
	Status ReadQRCode(ServerContext* context,const QRCode* qr_code,ReadQRCodeResponse* response) override;

private:
	std::vector<std::pair<std::string, std::string>> m_qr_code;

};


class CorosStreamServer 
{
public:
	CorosStreamServer(){RunServer();}
	void RunServer();

private:

};


#endif
