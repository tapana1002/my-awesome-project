/*************************************************************************
  > File Name: streaming.h
  > Author: 
  > Mail: 
  > Created Time: Wed 07 Jun 2017 03:36:54 PM PDT
 ************************************************************************/
#include "stream_server.h"


Status CorosStreamServerImpl::CorosStream(ServerContext* context,const StreamRequest* camera,ServerWriter<StreamData>* writer) 
{
    auto camera_id = camera->camera_id();
    std::cout << "coros streaming camera_id = " << camera_id << "\n";
    if(camera_id > 0)
    {
        StreamData f;
        for(int i=0; i<10; i++) 
        {
            f.set_x(i);
            f.set_y(i);
            f.set_z(i);
            f.set_r(i);
            f.set_g(i);
            f.set_b(i);
            writer->Write(f);
        }
    }
    return Status::OK;
}

Status CorosStreamServerImpl::ReadQRCode(ServerContext* context, const QRCode* qr_code, ReadQRCodeResponse* response)  
{
    auto qrcode = std::make_pair(qr_code->qr_code(), qr_code->time_stamp());
    m_qr_code.push_back(qrcode);
    std::cout << "server rcvd QR Code: " << m_qr_code[0].first << std::endl;
    std::cout << "server rcvd Time Stamp: " << m_qr_code[0].second << std::endl;
    response->set_qr_rcvd_status(0);
    return Status::OK;
}


void CorosStreamServer::RunServer() 
{
    std::string server_address("127.0.0.1:50051");
    CorosStreamServerImpl service;
    ServerBuilder builder;
    builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
    builder.RegisterService(&service);
    std::unique_ptr<Server> server(builder.BuildAndStart());
    std::cout << "Server listening on " << server_address << std::endl;
    server->Wait();
}


