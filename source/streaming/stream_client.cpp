/*************************************************************************
  > File Name: streaming.h
  > Author: 
  > Mail: 
  > Created Time: Wed 07 Jun 2017 03:36:54 PM PDT
 ************************************************************************/
#include "stream_client.h"


void CorosStreamClientImpl::CorosStream()
{
    StreamRequest stream_request;
    StreamData data;
    ClientContext context;

    stream_request.set_camera_id(1);

    std::unique_ptr<ClientReader<StreamData>> reader(stub_->CorosStream(&context, stream_request));
    while (reader->Read(&data)) 
    {
        std::cout << "Coros is Streaming... "
                  << data.x() << " " << data.y() << " " << data.z() << " " << data.r() << " " << data.g() << " " << data.b() << std::endl;
    }

    Status status = reader->Finish();
    if (status.ok()) 
    {
        std::cout << "Coros One Frame Streaming Succeeded." << std::endl;
    } 
    else 
    {
        std::cout << "Coros One Frame Streaming Failed." << std::endl;
    }
}

void CorosStreamClientImpl::ReadQRCode()
{
    QRCode qr_code;
    ReadQRCodeResponse response;
    ClientContext context;

    std::string code = "qr-code-123456";
    std::string time_stamp = "time-stamp-00:00:00";
    qr_code.set_qr_code(code);
    qr_code.set_time_stamp(time_stamp);

    Status status = stub_->ReadQRCode(&context, qr_code, &response);
    if(status.ok())
      std::cout << "Coros recieved QR Code with status "<< response.qr_rcvd_status() << std::endl;
}


void CorosStreamClient::RunClient()
{
    CorosStreamClientImpl guide(grpc::CreateChannel("127.0.0.1:50051", grpc::InsecureChannelCredentials()));
    guide.CorosStream();
    guide.ReadQRCode();
}



