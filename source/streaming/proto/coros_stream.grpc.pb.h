// Generated by the gRPC C++ plugin.
// If you make any local change, they will be lost.
// source: coros_stream.proto
#ifndef GRPC_coros_5fstream_2eproto__INCLUDED
#define GRPC_coros_5fstream_2eproto__INCLUDED

#include "coros_stream.pb.h"

#include <grpc++/impl/codegen/async_stream.h>
#include <grpc++/impl/codegen/async_unary_call.h>
#include <grpc++/impl/codegen/method_handler_impl.h>
#include <grpc++/impl/codegen/proto_utils.h>
#include <grpc++/impl/codegen/rpc_method.h>
#include <grpc++/impl/codegen/service_type.h>
#include <grpc++/impl/codegen/status.h>
#include <grpc++/impl/codegen/stub_options.h>
#include <grpc++/impl/codegen/sync_stream.h>

namespace grpc {
class CompletionQueue;
class Channel;
class RpcService;
class ServerCompletionQueue;
class ServerContext;
}  // namespace grpc

namespace corosstream {

class CorosStreamService final {
 public:
  class StubInterface {
   public:
    virtual ~StubInterface() {}
    virtual ::grpc::Status ReadQRCode(::grpc::ClientContext* context, const ::corosstream::QRCode& request, ::corosstream::ReadQRCodeResponse* response) = 0;
    std::unique_ptr< ::grpc::ClientAsyncResponseReaderInterface< ::corosstream::ReadQRCodeResponse>> AsyncReadQRCode(::grpc::ClientContext* context, const ::corosstream::QRCode& request, ::grpc::CompletionQueue* cq) {
      return std::unique_ptr< ::grpc::ClientAsyncResponseReaderInterface< ::corosstream::ReadQRCodeResponse>>(AsyncReadQRCodeRaw(context, request, cq));
    }
    std::unique_ptr< ::grpc::ClientReaderInterface< ::corosstream::StreamData>> CorosStream(::grpc::ClientContext* context, const ::corosstream::StreamRequest& request) {
      return std::unique_ptr< ::grpc::ClientReaderInterface< ::corosstream::StreamData>>(CorosStreamRaw(context, request));
    }
    std::unique_ptr< ::grpc::ClientAsyncReaderInterface< ::corosstream::StreamData>> AsyncCorosStream(::grpc::ClientContext* context, const ::corosstream::StreamRequest& request, ::grpc::CompletionQueue* cq, void* tag) {
      return std::unique_ptr< ::grpc::ClientAsyncReaderInterface< ::corosstream::StreamData>>(AsyncCorosStreamRaw(context, request, cq, tag));
    }
  private:
    virtual ::grpc::ClientAsyncResponseReaderInterface< ::corosstream::ReadQRCodeResponse>* AsyncReadQRCodeRaw(::grpc::ClientContext* context, const ::corosstream::QRCode& request, ::grpc::CompletionQueue* cq) = 0;
    virtual ::grpc::ClientReaderInterface< ::corosstream::StreamData>* CorosStreamRaw(::grpc::ClientContext* context, const ::corosstream::StreamRequest& request) = 0;
    virtual ::grpc::ClientAsyncReaderInterface< ::corosstream::StreamData>* AsyncCorosStreamRaw(::grpc::ClientContext* context, const ::corosstream::StreamRequest& request, ::grpc::CompletionQueue* cq, void* tag) = 0;
  };
  class Stub final : public StubInterface {
   public:
    Stub(const std::shared_ptr< ::grpc::ChannelInterface>& channel);
    ::grpc::Status ReadQRCode(::grpc::ClientContext* context, const ::corosstream::QRCode& request, ::corosstream::ReadQRCodeResponse* response) override;
    std::unique_ptr< ::grpc::ClientAsyncResponseReader< ::corosstream::ReadQRCodeResponse>> AsyncReadQRCode(::grpc::ClientContext* context, const ::corosstream::QRCode& request, ::grpc::CompletionQueue* cq) {
      return std::unique_ptr< ::grpc::ClientAsyncResponseReader< ::corosstream::ReadQRCodeResponse>>(AsyncReadQRCodeRaw(context, request, cq));
    }
    std::unique_ptr< ::grpc::ClientReader< ::corosstream::StreamData>> CorosStream(::grpc::ClientContext* context, const ::corosstream::StreamRequest& request) {
      return std::unique_ptr< ::grpc::ClientReader< ::corosstream::StreamData>>(CorosStreamRaw(context, request));
    }
    std::unique_ptr< ::grpc::ClientAsyncReader< ::corosstream::StreamData>> AsyncCorosStream(::grpc::ClientContext* context, const ::corosstream::StreamRequest& request, ::grpc::CompletionQueue* cq, void* tag) {
      return std::unique_ptr< ::grpc::ClientAsyncReader< ::corosstream::StreamData>>(AsyncCorosStreamRaw(context, request, cq, tag));
    }

   private:
    std::shared_ptr< ::grpc::ChannelInterface> channel_;
    ::grpc::ClientAsyncResponseReader< ::corosstream::ReadQRCodeResponse>* AsyncReadQRCodeRaw(::grpc::ClientContext* context, const ::corosstream::QRCode& request, ::grpc::CompletionQueue* cq) override;
    ::grpc::ClientReader< ::corosstream::StreamData>* CorosStreamRaw(::grpc::ClientContext* context, const ::corosstream::StreamRequest& request) override;
    ::grpc::ClientAsyncReader< ::corosstream::StreamData>* AsyncCorosStreamRaw(::grpc::ClientContext* context, const ::corosstream::StreamRequest& request, ::grpc::CompletionQueue* cq, void* tag) override;
    const ::grpc::RpcMethod rpcmethod_ReadQRCode_;
    const ::grpc::RpcMethod rpcmethod_CorosStream_;
  };
  static std::unique_ptr<Stub> NewStub(const std::shared_ptr< ::grpc::ChannelInterface>& channel, const ::grpc::StubOptions& options = ::grpc::StubOptions());

  class Service : public ::grpc::Service {
   public:
    Service();
    virtual ~Service();
    virtual ::grpc::Status ReadQRCode(::grpc::ServerContext* context, const ::corosstream::QRCode* request, ::corosstream::ReadQRCodeResponse* response);
    virtual ::grpc::Status CorosStream(::grpc::ServerContext* context, const ::corosstream::StreamRequest* request, ::grpc::ServerWriter< ::corosstream::StreamData>* writer);
  };
  template <class BaseClass>
  class WithAsyncMethod_ReadQRCode : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service *service) {}
   public:
    WithAsyncMethod_ReadQRCode() {
      ::grpc::Service::MarkMethodAsync(0);
    }
    ~WithAsyncMethod_ReadQRCode() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status ReadQRCode(::grpc::ServerContext* context, const ::corosstream::QRCode* request, ::corosstream::ReadQRCodeResponse* response) final override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
    void RequestReadQRCode(::grpc::ServerContext* context, ::corosstream::QRCode* request, ::grpc::ServerAsyncResponseWriter< ::corosstream::ReadQRCodeResponse>* response, ::grpc::CompletionQueue* new_call_cq, ::grpc::ServerCompletionQueue* notification_cq, void *tag) {
      ::grpc::Service::RequestAsyncUnary(0, context, request, response, new_call_cq, notification_cq, tag);
    }
  };
  template <class BaseClass>
  class WithAsyncMethod_CorosStream : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service *service) {}
   public:
    WithAsyncMethod_CorosStream() {
      ::grpc::Service::MarkMethodAsync(1);
    }
    ~WithAsyncMethod_CorosStream() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status CorosStream(::grpc::ServerContext* context, const ::corosstream::StreamRequest* request, ::grpc::ServerWriter< ::corosstream::StreamData>* writer) final override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
    void RequestCorosStream(::grpc::ServerContext* context, ::corosstream::StreamRequest* request, ::grpc::ServerAsyncWriter< ::corosstream::StreamData>* writer, ::grpc::CompletionQueue* new_call_cq, ::grpc::ServerCompletionQueue* notification_cq, void *tag) {
      ::grpc::Service::RequestAsyncServerStreaming(1, context, request, writer, new_call_cq, notification_cq, tag);
    }
  };
  typedef WithAsyncMethod_ReadQRCode<WithAsyncMethod_CorosStream<Service > > AsyncService;
  template <class BaseClass>
  class WithGenericMethod_ReadQRCode : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service *service) {}
   public:
    WithGenericMethod_ReadQRCode() {
      ::grpc::Service::MarkMethodGeneric(0);
    }
    ~WithGenericMethod_ReadQRCode() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status ReadQRCode(::grpc::ServerContext* context, const ::corosstream::QRCode* request, ::corosstream::ReadQRCodeResponse* response) final override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
  };
  template <class BaseClass>
  class WithGenericMethod_CorosStream : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service *service) {}
   public:
    WithGenericMethod_CorosStream() {
      ::grpc::Service::MarkMethodGeneric(1);
    }
    ~WithGenericMethod_CorosStream() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status CorosStream(::grpc::ServerContext* context, const ::corosstream::StreamRequest* request, ::grpc::ServerWriter< ::corosstream::StreamData>* writer) final override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
  };
  template <class BaseClass>
  class WithStreamedUnaryMethod_ReadQRCode : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service *service) {}
   public:
    WithStreamedUnaryMethod_ReadQRCode() {
      ::grpc::Service::MarkMethodStreamed(0,
        new ::grpc::StreamedUnaryHandler< ::corosstream::QRCode, ::corosstream::ReadQRCodeResponse>(std::bind(&WithStreamedUnaryMethod_ReadQRCode<BaseClass>::StreamedReadQRCode, this, std::placeholders::_1, std::placeholders::_2)));
    }
    ~WithStreamedUnaryMethod_ReadQRCode() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable regular version of this method
    ::grpc::Status ReadQRCode(::grpc::ServerContext* context, const ::corosstream::QRCode* request, ::corosstream::ReadQRCodeResponse* response) final override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
    // replace default version of method with streamed unary
    virtual ::grpc::Status StreamedReadQRCode(::grpc::ServerContext* context, ::grpc::ServerUnaryStreamer< ::corosstream::QRCode,::corosstream::ReadQRCodeResponse>* server_unary_streamer) = 0;
  };
  typedef WithStreamedUnaryMethod_ReadQRCode<Service > StreamedUnaryService;
  template <class BaseClass>
  class WithSplitStreamingMethod_CorosStream : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service *service) {}
   public:
    WithSplitStreamingMethod_CorosStream() {
      ::grpc::Service::MarkMethodStreamed(1,
        new ::grpc::SplitServerStreamingHandler< ::corosstream::StreamRequest, ::corosstream::StreamData>(std::bind(&WithSplitStreamingMethod_CorosStream<BaseClass>::StreamedCorosStream, this, std::placeholders::_1, std::placeholders::_2)));
    }
    ~WithSplitStreamingMethod_CorosStream() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable regular version of this method
    ::grpc::Status CorosStream(::grpc::ServerContext* context, const ::corosstream::StreamRequest* request, ::grpc::ServerWriter< ::corosstream::StreamData>* writer) final override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
    // replace default version of method with split streamed
    virtual ::grpc::Status StreamedCorosStream(::grpc::ServerContext* context, ::grpc::ServerSplitStreamer< ::corosstream::StreamRequest,::corosstream::StreamData>* server_split_streamer) = 0;
  };
  typedef WithSplitStreamingMethod_CorosStream<Service > SplitStreamedService;
  typedef WithStreamedUnaryMethod_ReadQRCode<WithSplitStreamingMethod_CorosStream<Service > > StreamedService;
};

}  // namespace corosstream


#endif  // GRPC_coros_5fstream_2eproto__INCLUDED
