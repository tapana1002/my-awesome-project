/*
* Copyright Daimler, 2017
* All Rights Reserved
* UNPUBLISHED, LICENSED SOFTWARE.
* CONFIDENTIAL AND PROPRIETARY INFORMATION
* WHICH IS THE PROPERTY OF Daimler.
*
* @file   ImageProcessing.h
* @date   June, 2017
* @brief  Image Processing related static functions
*
*/

#ifndef _IMAGEPROCESSING_H_
#define _IMAGEPROCESSING_H_

#include "Common.h"
#include "InventoryManager.h"
#include "RealsenseCamera.h"
#include "OrbbecCamera.h"

/**
*	The Box class contains all information and actions needed for a box package
*/
namespace Imgproc
{
	void search_for_movement(cv::Mat thresholded, cv::Mat cameraFeed);
	std::vector<BarCode> scan_image_zbar(cv::Mat source_image);
	void scan_image_rs_zbar(RealsenseCamera* cam, cv::Mat source_image);
	void scan_image_orb_zbar(OrbbecCamera*cam, cv::Mat source_image);
	cv::Mat apply_background_subtraction(cv::Mat frame1, cv::Mat frame2);

	// Kernel or Convolusion Matrix Size
	const static int MASK = 21;
	//Background Subtraction threshold
	const static int THRESHOLD = 15;
};

#endif