/*
* Copyright Daimler, 2017
* All Rights Reserved
* UNPUBLISHED, LICENSED SOFTWARE.
* CONFIDENTIAL AND PROPRIETARY INFORMATION
* WHICH IS THE PROPERTY OF Daimler.
*
* @file   ImageProcessing.h
* @date   June, 2017
* @brief  Image Processing related static functions
*
*/

#include "ImageProcessing.h"

using namespace cv;
using namespace std;
using namespace zbar;

// Function to draw bounding boxes around the moving objects
void Imgproc::search_for_movement(Mat thresholded, Mat cameraFeed)
{
	Rect boundRect = Rect(0,0,0,0);
	int object[2] = {0,0};
    bool objectDetected = false;
    Mat temp;
    thresholded.copyTo(temp);
    
    // Find all contours in the thresholded image
    vector< vector<Point> > contours;
    vector<Vec4i> hierarchy;
    
    findContours(temp, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
    
    if(contours.size() > 0)
        objectDetected = true;
    else
        objectDetected = false;
    
    // Retain the largest contour
    if(objectDetected)
    {
        double largestArea = 0;
        //int largestContourIndex = 0;
        for (uint64_t i = 0; i < contours.size(); i++)
        {
            double area = contourArea(contours[i], false);
            if (area > largestArea)
            {
                largestArea = area;
                //largestContourIndex = i;
                boundRect = boundingRect(contours[i]);
            }
        }
        
        int xpos = boundRect.x + boundRect.width/2;
        int ypos = boundRect.y + boundRect.height/2;
        
        object[0] = xpos;
        object[1] = ypos;
        cout << "coordinates: " << xpos << "\t" << ypos << endl;
    }
    
    int x = object[0];
    int y = object[1];
    
    // If area of the contour is large enough, draw a bounding box around it
    if(boundRect.area()>180 && boundRect.width>40 && boundRect.height>40)
    {
        rectangle(cameraFeed, boundRect, Scalar(255,0,0),3,8,0);
        line(cameraFeed,Point(x,y),Point(x,y-5),Scalar(0,255,0),3);
        line(cameraFeed,Point(x,y),Point(x,y+5),Scalar(0,255,0),3);
        line(cameraFeed,Point(x,y),Point(x-5,y),Scalar(0,255,0),3);
        line(cameraFeed,Point(x,y),Point(x+5,y),Scalar(0,255,0),3);
    }
}

cv::Mat Imgproc::apply_background_subtraction(cv::Mat frame1, cv::Mat frame2)
{
	Mat difference, gray1, gray2, thresholded;

	//convert to grayscale
    cvtColor(frame1, gray1, COLOR_BGR2GRAY);
    cvtColor(frame2, gray2, COLOR_BGR2GRAY);
    
    // Compute the difference image and apply blurring along with thresholding
    difference = abs(gray1 - gray2);
    GaussianBlur(difference, difference, Size(MASK, MASK), 0);
    threshold(difference,thresholded, THRESHOLD, 255, THRESH_BINARY);

    return thresholded;
}

vector<BarCode> Imgproc::scan_image_zbar(Mat source_image)
{
	ImageScanner scanner;
	vector<BarCode> codes;
	Mat grayscale_frame;

	//start the zbar scanner with the default config
    scanner.set_config(ZBAR_CODE128, ZBAR_CFG_ENABLE, 1); 

    //Make the frame grayscale
	cvtColor(source_image, grayscale_frame, CV_BGR2GRAY);

	// Obtain frame data and construct a zbar Image object
    int width = grayscale_frame.cols;
    int height = grayscale_frame.rows;
    uchar *raw = (uchar *)(grayscale_frame.data);
	Image image(width, height, "Y800", raw, width * height);	

	scanner.scan(image);
	for (Image::SymbolIterator symbol = image.symbol_begin(); symbol != image.symbol_end(); ++symbol) 
	{
		BarCode code;
        
        code.m_zbar_type = symbol->get_type();
        code.m_zbar_type_name = symbol->get_type_name();
        code.m_zbar_addon_name = symbol->get_addon_name();
        code.m_zbar_data = symbol->get_data();
        code.m_zbar_data_length = symbol->get_data_length();
        code.m_zbar_location_size = symbol->get_location_size();


        for(int i = 0; i < code.m_zbar_location_size; i++)
        {
        	code.m_zbar_location_x.push_back(symbol->get_location_x(i));
        	code.m_zbar_location_y.push_back(symbol->get_location_y(i));
        }

        codes.push_back(code);
    } 
    image.set_data(NULL, 0);

    return codes;
}

void Imgproc::scan_image_rs_zbar(RealsenseCamera* cam, Mat source_image)
{
	ImageScanner scanner;
	vector<BarCode> codes;
	Mat grayscale_frame;

	//start the zbar scanner with the default config
    scanner.set_config(ZBAR_NONE, ZBAR_CFG_ENABLE, 1); 

    //Make the frame grayscale
	cvtColor(source_image, grayscale_frame, CV_BGR2GRAY);

	// Obtain frame data and construct a zbar Image object
    int width = grayscale_frame.cols;
    int height = grayscale_frame.rows;
    uchar *raw = (uchar *)(grayscale_frame.data);
	Image image(width, height, "Y800", raw, width * height);	

	LOG(INFO) << "RS Camera scanning";
	scanner.scan(image);

	for (Image::SymbolIterator symbol = image.symbol_begin(); symbol != image.symbol_end(); ++symbol) 
	{
		BarCode code;
        
		LOG(INFO) << "rs got codes";

        code.m_zbar_type = symbol->get_type();
        code.m_zbar_type_name = symbol->get_type_name();
        code.m_zbar_addon_name = symbol->get_addon_name();
        code.m_zbar_data = symbol->get_data();
        code.m_zbar_data_length = symbol->get_data_length();
        code.m_zbar_location_size = symbol->get_location_size();

        LOG(INFO) << "Realsense Camera " << cam->m_camera->m_serial <<" scanned(" << code.m_zbar_type_name <<"): " << code.m_zbar_data;

        for(int i = 0; i < code.m_zbar_location_size; i++)
        {
        	code.m_zbar_location_x.push_back(symbol->get_location_x(i));
        	code.m_zbar_location_y.push_back(symbol->get_location_y(i));
        }

        codes.push_back(code);
    } 
    image.set_data(NULL, 0);

    cam->m_zbar_enabled = false;
}

void Imgproc::scan_image_orb_zbar(OrbbecCamera* cam, Mat source_image)
{
	ImageScanner scanner;
	vector<BarCode> codes;
	Mat grayscale_frame;

	//start the zbar scanner with the default config
    scanner.set_config(ZBAR_NONE, ZBAR_CFG_ENABLE, 1); 

    //Make the frame grayscale
	cvtColor(source_image, grayscale_frame, CV_BGR2GRAY);

	// Obtain frame data and construct a zbar Image object
    int width = grayscale_frame.cols;
    int height = grayscale_frame.rows;
    uchar *raw = (uchar *)(grayscale_frame.data);
	Image image(width, height, "Y800", raw, width * height);	

	LOG(INFO) << "Orbbec Camera scanning";
	scanner.scan(image);

	for (Image::SymbolIterator symbol = image.symbol_begin(); symbol != image.symbol_end(); ++symbol) 
	{
		BarCode code;

        LOG(INFO) << "Orbbec Camera got codes";
        code.m_zbar_type = symbol->get_type();
        code.m_zbar_type_name = symbol->get_type_name();
        code.m_zbar_addon_name = symbol->get_addon_name();
        code.m_zbar_data = symbol->get_data();
        code.m_zbar_data_length = symbol->get_data_length();
        code.m_zbar_location_size = symbol->get_location_size();

        LOG(INFO) << "Orbbec Camera " << cam->m_camera_serial <<" scanned(" << code.m_zbar_type_name <<"): " << code.m_zbar_data;

        for(int i = 0; i < code.m_zbar_location_size; i++)
        {
        	code.m_zbar_location_x.push_back(symbol->get_location_x(i));
        	code.m_zbar_location_y.push_back(symbol->get_location_y(i));
        }

        codes.push_back(code);
    } 
    image.set_data(NULL, 0);

    cam->m_zbar_enabled = false;
}