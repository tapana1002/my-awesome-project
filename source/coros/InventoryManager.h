/*
* Copyright Daimler, 2017
* All Rights Reserved
* UNPUBLISHED, LICENSED SOFTWARE.
* CONFIDENTIAL AND PROPRIETARY INFORMATION
* WHICH IS THE PROPERTY OF Daimler.
*
* @file   InventoryManager.h
* @date   June, 2017
* @brief  Inventory Manager Class definitions and related functions
*
*/

#ifndef _INVENTORYMANAGER_H_
#define _INVENTORYMANAGER_H_

#include "Common.h"
#include "Box.h"

/**
*	The Box class contains all information and actions needed for a box package
*/
class InventoryManager{
	public:
		InventoryManager(); 
		virtual ~InventoryManager();
	
	private:

};

class BarCode{
	public:
		BarCode(); 
		virtual ~BarCode();
		
		std::vector<int> m_zbar_location_x;
		std::vector<int> m_zbar_location_y;
		int m_zbar_location_size;
		int m_zbar_data_length;
		std::string m_zbar_type_name;
		std::string m_zbar_addon_name;
		std::string m_zbar_data;
		zbar::zbar_symbol_type_t m_zbar_type;
	private:

};

#endif