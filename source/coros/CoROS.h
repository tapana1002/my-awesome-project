/* ========================================
*
* Copyright Daimler, 2017
* All Rights Reserved
* UNPUBLISHED, LICENSED SOFTWARE.
*
* CONFIDENTIAL AND PROPRIETARY INFORMATION
* WHICH IS THE PROPERTY OF Daimler.
*
* ========================================
*/
#ifndef _COROS_H_
#define _COROS_H_
#include "stream_server.h"
#include "stream_client.h"
#include "OrbbecManager.h"
#include "RealsenseManager.h"
#include "InventoryManager.h"
#include "ImageProcessing.h"
#include "usb_util.h"
#include "zbar.h"

/**
*	CoROS oversee all the cameras and monitors their working status
*/
class CoROS{
	public:
		CoROS(); 
		virtual ~CoROS();
		openni::VideoStream** m_openni_streams;
		std::thread  		  m_orbbec_manager_thread;
		std::thread  		  m_realsense_manager_thread;
		bool 		 		  m_using_orbbec_cameras;
		bool 		 		  m_using_realsense_cameras;
		
		static void coros_shutdown(int sig);
		static void cmd_parser(int argc, char** argv);
		int get_CV_camera_Count();
		int basic_capture_mode_loop();
		int run_mode_door_tracker();
	
	private:
		OrbbecManager *orb_manager;
		RealsenseManager *rs_manager;
		CorosStreamServer*	stream_server;
		CorosStreamClient*  stream_client;

		bool Init_Cameras();
		bool start_pbl();
		bool start_grpc();
		bool start_extra_cam();
};

const std::string coros_str = 
	"   ____      ____   ___  ____  \n"
	"  / ___|___ |  _ \\ / _ \\/ ___| \n"
	" | |   / _ \\| |_) | | | \\___ \\ \n"
	" | |__| (_) |  _ <| |_| |___)|\n"
	"  \\____\\___/|_| \\_\\___/|_____/ \n"
	"                               \n";

extern int  g_current_camera_count; //Count for enabled Camera devices

#endif
