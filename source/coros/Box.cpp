/*
* Copyright Daimler, 2017
* All Rights Reserved
* UNPUBLISHED, LICENSED SOFTWARE.
* CONFIDENTIAL AND PROPRIETARY INFORMATION
* WHICH IS THE PROPERTY OF Daimler.
*
* @file   Box.cpp
* @date   June, 2017
* @brief  Box Class definitions and related functions
*
* This file defines the functions that the orbbec manager thread uses to create/modify the orbbec devices.
* Each device has a thread and they are all managed by the orbbec manager thread.
*/

#include "Box.h"

/*
 * @name    Box
 * @brief   
 * @Descr 
 * @param1 
 */
Box::Box( int box_id){
	m_box_id = box_id
} 

/*
 * @name    ~Box
 * @brief   
 * @Descr 
 * @param1 
 */
Box::~Box()
{

}

/*
 * @name    get_box_id
 * @brief   
 * @Descr 
 * @param1 
 */
int Box::get_box_id()
{
	return m_box_id;
}

/*
 * @name    get_box_volume
 * @brief   
 * @Descr 
 * @param1 
 */
double Box::get_box_volume()
{
	return m_box_volume;
}

/*
 * @name    get_vector_world_pos
 * @brief   
 * @Descr 
 * @param1 
 */
void Box::get_vector_world_pos(float* world_pos)
{
	world_pos[0] = m_vector_world_pos[0]; 
	world_pos[1] = m_vector_world_pos[1];
	world_pos[2] = m_vector_world_pos[2];
}

/*
 * @name    get_box_length
 * @brief   
 * @Descr 
 * @param1 
 */
float Box::get_box_length()
{
	return m_box_length;
}

/*
 * @name    get_box_width
 * @brief   
 * @Descr 
 * @param1 
 */
float Box::get_box_width()
{
	return m_box_width;
}

/*
 * @name    get_box_height
 * @brief   
 * @Descr 
 * @param1 
 */
float Box::get_box_height()
{
	return m_box_height;
}

/*
 * @name    get_box_status
 * @brief   
 * @Descr 
 * @param1 
 */
box_status Box::get_box_status()
{
	return m_curr_box_status;
}

/*
 * @name    get_barcode_data
 * @brief   
 * @Descr 
 * @param1 
 */
std::string Box::get_barcode_data()
{
	return m_barcode_data;
}

/*
 * @name    set_box_volume
 * @brief   
 * @Descr 
 * @param1 
 */
bool Box::set_box_volume(double vol)
{
	m_box_volume = vol;
}

/*
 * @name    set_vector_world_pos
 * @brief   
 * @Descr 
 * @param1 
 */
bool Box::set_vector_world_pos(float* world_pos)
{
	m_vector_world_pos[0] = world_pos[0]; //x
	m_vector_world_pos[1] = world_pos[1]; //y
	m_vector_world_pos[2] = world_pos[2]; //z
} 

/*
 * @name    set_box_length
 * @brief   
 * @Descr 
 * @param1 
 */
bool Box::set_box_length(float length)
{
	m_box_length = length;
}

/*
 * @name    set_box_width
 * @brief   
 * @Descr 
 * @param1 
 */
bool Box::set_box_width(float width)
{
	m_box_width = width;
}

/*
 * @name    set_box_height
 * @brief   
 * @Descr 
 * @param1 
 */
bool Box::set_box_height(float height)
{
	m_box_height = height;
}

/*
 * @name    set_box_status
 * @brief   
 * @Descr 
 * @param1 
 */
bool Box::set_box_status(box_status status)
{
	m_curr_box_status = status;
}

/*
 * @name    set_barcode_data
 * @brief   
 * @Descr 
 * @param1 
 */
bool Box::set_barcode_data(std::string data)
{
	m_barcode_data = data;
}

