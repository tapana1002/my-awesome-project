/* ========================================
*
* Copyright Daimler, 2017
* All Rights Reserved
* UNPUBLISHED, LICENSED SOFTWARE.
*
* CONFIDENTIAL AND PROPRIETARY INFORMATION
* WHICH IS THE PROPERTY OF Daimler.
*
* ========================================
*/
#include "CoROS.h"

using namespace std;
using namespace zbar;
using namespace cv;

extern COMMAND_LINE_ARGS g_command_line_args;
/**
*	CoROS constructor: initialize all the cameras and start Pick-By-Light module.
*/
CoROS::CoROS()
{
	Init_Cameras();
	start_extra_cam();
	start_pbl();
	start_grpc();
}

/**
*	destructor of CoROS, delete allocated memory resources
*/
CoROS::~CoROS()
{

}

/**
*	start pick-by-light module
*/
bool CoROS::start_pbl()
{
	return 0;
}

/**
*	\brief start_grpc.
*/
bool CoROS::start_grpc()
{
#if(COROS_STREAM == COROS_STREAM_SERVER)
	stream_server = new CorosStreamServer();
#else
	stream_client = new CorosStreamClient();
#endif 
	return 0;
}

/**
*	Start basic usb color camera modes
*/
bool CoROS::start_extra_cam()
{
	if(get_CV_camera_Count() > 1)
	{
		if(g_command_line_args.basic_cv_mode_enabled)
		{	
			LOG(INFO) << "Starting basic image/video capture functions.";
			if(basic_capture_mode_loop() == 0)
			{
				LOG(INFO) << "Shutting down CoROS....";
		    	openni::OpenNI::shutdown();
		    	libusb_exit(NULL);  
		    	exit(EXIT_SUCCESS);
			}
		} else if(g_command_line_args.simple_box_tracker_demo_enabled)
		{
			LOG(INFO) << "Running coros simple box tracker demo.";

		} 
		else
		{
			LOG(INFO) << "Nothing specified for additional usb color camera detected.";
		}	
	}
	return 0;
}

/*
 * \Descr safe exit for a CTRL-C 
 * \param1
 */
void CoROS::coros_shutdown(int sig)
{
  LOG(INFO) << "Shutting down CoROS....";
  openni::OpenNI::shutdown();
  libusb_exit(NULL);  
  exit(EXIT_SUCCESS);
}

/*
 * \Descr parse the command line arguments 
 * \param1
 */
void CoROS::cmd_parser(int argc, char** argv)
{
	int arg = 0;
	
	for(arg=1; arg<argc; arg++)
	{
		string str = argv[arg];
		size_t pos = str.find("=");
		
		string option = str.substr(0,pos);
		string value = str.substr(pos+1);
		
		if("-rgbdepth" == option)
		{
			g_command_line_args.rgb_depth_enabled = true;
		}
		else if("-rgb" == option)
		{
			g_command_line_args.rgb_enabled = true;
		}
		else if("-ir" == option)
		{
			g_command_line_args.ir_enabled = true;	
		}
		else if("-csvd" == option)
		{
			g_command_line_args.csv_depth_record_enabled = true;
		}
		else if("-csvpc" == option)
		{
			g_command_line_args.csv_pointcloud_record_enabled = true;
		}
		else if("-csvpcc" == option)
		{
			g_command_line_args.rgb_enabled = true;
			g_command_line_args.csv_pointcloudcolor_record_enabled = true;
		}
		else if("-jsond" == option)
		{
			g_command_line_args.json_depth_record_enabled = true;
		}
		else if("-jsonpc" == option)
		{
			g_command_line_args.json_pointcloud_record_enabled = true;
		}
		else if("-jsonpcc" == option)
		{
			g_command_line_args.rgb_enabled = true;
			g_command_line_args.json_pointcloudcolor_record_enabled = true;
		}
		else if("-savecolor" == option)
		{
			g_command_line_args.rgb_enabled = true;
			g_command_line_args.rgb_save_jpeg_enabled = true;
		}
		else if("-savedepth" == option)
		{
			g_command_line_args.depth_save_jpeg_enabled = true;
		}
		else if("-saveir" == option)
		{
			g_command_line_args.ir_enabled = true;
			g_command_line_args.ir_save_jpeg_enabled = true;
		}
		else if("-savevideo" == option)
		{
			g_command_line_args.rgb_enabled = true;
			g_command_line_args.rgb_save_avi_enabled = true;
		}
		else if("-zbar" == option)
		{
			g_command_line_args.rgb_enabled = true;
			g_command_line_args.zbar_enabled = true;
		}
		else if("-plypc" == option)
		{
			g_command_line_args.ply_pointcloud_record_enabled = true;
		}
		else if("-plypcc" == option)
		{
			g_command_line_args.rgb_enabled = true;
			g_command_line_args.ply_pointcloudcolor_record_enabled = true;
		}
		else if("-singlecamcv" == option)
		{
			g_command_line_args.basic_cv_mode_enabled = true;
		}
		else if("-simpletracker" == option)
		{
			g_command_line_args.rgb_enabled = true;
			g_command_line_args.simple_box_tracker_demo_enabled = true;
		}
	}
}

/**
*	\brief Initializes all the cameras.
*/
bool CoROS::Init_Cameras()
{
	uvc_context_t *ctx;
	int status = 0;

	/* Initialize a UVC service context. Libuvc will set up its own libusb
    * context. Replace NULL with a libusb_context pointer to run libuvc
    * from an existing libusb context. This is for ASTRA PROS only */
    status = uvc_init(&ctx, NULL);
    if (status < 0) {
      LOG(ERROR) << "Unable to initialize uvc library";
      exit(1);
    }

    if( get_realsense_camera_count() > 0 )
    {
		RealsenseManager *rs_manager = new RealsenseManager();
		m_realsense_manager_thread = std::thread(&RealsenseManager::realsense_manager_loop, rs_manager); 
	} else {
		LOG(INFO) << "No RealSense cameras found, not starting RealSense Manager thread.";
		m_using_realsense_cameras = false;
	}

	if( get_orbbec_camera_count() > 0 )
	{	
		if( (g_command_line_args.rgb_enabled && g_command_line_args.ir_enabled) )
		{
			LOG(ERROR) << "Can't collect ir and rgb or ir and depth from orbbec cameras simultaneously, not starting orbbec thread.";
		} else {
			orb_manager = new OrbbecManager();
			m_orbbec_manager_thread = std::thread(&OrbbecManager::orbbec_manager_loop, orb_manager);
			orb_manager->m_ctx = ctx;
			m_using_orbbec_cameras = true;
		}
	} else {
		LOG(INFO) << "No Orbbec cameras found, not starting Orbbec Manager thread.";
		m_using_orbbec_cameras = false;
	}
	
	return true;
}

int CoROS::get_CV_camera_Count()
{
	int count = 0;
    while (true)
    {
        cv::VideoCapture cap;
        try{
        	cap.open(count);
        }catch(...){}
        if (!cap.isOpened())
        {
            break;
        }
        else
        {
            count++;
            cap.release();
        }
    }
    return count;
}

int CoROS::basic_capture_mode_loop()
{
	cv::VideoCapture cap;
	cv::VideoWriter *video_ptr = NULL;
	int key, frame_width, frame_height;

	static int file_counter = 0;
	string video_file_name = "corosvid-";
	video_file_name.append(std::to_string(file_counter));
	video_file_name.append(".avi");
	string photo_file_name = "corosphoto-";
	photo_file_name.append(std::to_string(file_counter));
	photo_file_name.append(".jpg");
	int device_index = 0;
	int mode = 0,
		width = 0,
		height = 0;
	int delay_sec = 0;
	 
	
	cout << "Input USB Video device index: ";
	cin >> device_index;

	cap.open(device_index);
	if (!cap.isOpened())
    {
        LOG(ERROR) << "Unable to open video device, try running 'ls /dev/video*' to see available devices";
      	exit(1);
    }

	LOG(INFO) << "Successfully opened video device " << device_index << " .\n";

	cout << "\nWhat height would you like to capture images(default-480):";
	cin >> height;
	if(height  < 480)
	{
		height=480;
	}
	cout << "\nWhat width would you like to capture images(default-640):";
	cin >> width;
	if(width < 640)
	{
		width=640;
	}

	cout << "The following capture modes are available (default:0):" << endl;
	cout << "\t0 - Capture photos by pressing 'c'." << endl;
	cout << "\t1 - Capture video files, pressing 's' will save the file and start a new file." << endl;
	cout << "\t2 - Capture photos continuously with a given interval." << endl;
	cout << "\t3 - Do object tracking with background subtraction with blurring by a given threshold." << endl;
	cout << "\t4 - Scan for barcodes using zbar, press 'c' for captures." << endl;
	cout << "Pressing (ESC) will exit the CoROS App, enter mode please:";
	cin >> mode;

	cap.set(cv::CAP_PROP_FRAME_HEIGHT, height);
    cap.set(cv::CAP_PROP_FRAME_WIDTH,  width);
    cap.set(CV_CAP_PROP_FOURCC ,CV_FOURCC('M', 'J', 'P', 'G') );

	if(mode == 1)
	{
		
		frame_width=   cap.get(CV_CAP_PROP_FRAME_WIDTH);
   		frame_height=   cap.get(CV_CAP_PROP_FRAME_HEIGHT);
   		video_ptr = new cv::VideoWriter(video_file_name, CV_FOURCC('M', 'J', 'P', 'G'), 30, cv::Size(frame_width,frame_height), true);
		LOG(INFO) << "Starting video capture mode, rember to press 's' when you need to save a clip." << endl;
	} else if (mode == 2)
	{
		cout << "Please enter delay in seconds for photo capturing:";
		cin >> delay_sec;
	}

	while(1)
	{
		key = cv::waitKey(5);
		if( key == 27)
		{
			cap.release();
			return 0;
		}

		cv::Mat frame;
		cap >> frame; 

		switch(mode)
		{
			case 0:
				//Capture photos by pressing 'c'.
				namedWindow("Frame", WINDOW_NORMAL);
				cv::resizeWindow("Frame", 640, 480);
			    imshow( "Frame", frame );
			    if(key == 'c')
				{
					imwrite(photo_file_name, frame);
					photo_file_name = "corosphoto-";
					photo_file_name.append(std::to_string(file_counter));
					photo_file_name.append(".jpg");
					file_counter++;
				}
				break;
			case 1:
				//Capture video files, pressing 's' will save the file and start a new file.
				if(key == 's')
				{
					video_ptr->release();
					free(video_ptr);
					file_counter++;
					video_file_name = "corosvid-";
					video_file_name.append(std::to_string(file_counter));
					video_file_name.append(".avi");
					LOG(INFO) << "Saved Video, starting new video..." << endl;
					video_ptr = new cv::VideoWriter(video_file_name, CV_FOURCC('M', 'J', 'P', 'G'), 30, cv::Size(frame_width,frame_height), true);
				} else 
				{
			        video_ptr->write(frame);
			        imshow( "Frame", frame );
				}	
				break;
			case 2: 
				//Capture photos continuously with a given interval.
				namedWindow("Frame", WINDOW_NORMAL);
				imshow( "Frame", frame );
				imwrite(photo_file_name, frame);
				photo_file_name = "corosphoto-";
				photo_file_name.append(std::to_string(file_counter));
				photo_file_name.append(".jpg");
				file_counter++;
				sleep(delay_sec);
				break;
			case 3:
				{ 
					//Do object tracking with background subtraction with blurring by a given threshold.
					cv::Mat frame2, thresholded;
					cap >> frame2;

					namedWindow("Tracking video",CV_WINDOW_AUTOSIZE);

					//apply background subtraction
					thresholded = Imgproc::apply_background_subtraction(frame, frame2);

					 // Apply morphological operations
			        Mat structuringElement7x7 = getStructuringElement(MORPH_RECT, Size(13, 13));
			        Mat structuringElement9x9 = getStructuringElement(MORPH_RECT, Size(9, 9));
			        
			        dilate(thresholded, thresholded, structuringElement9x9);
			        erode(thresholded, thresholded, structuringElement9x9);
			        dilate(thresholded, thresholded, structuringElement7x7);

			        //look for objects
			        Imgproc::search_for_movement(thresholded, frame);

			        // Display the current frame and the results of thresholding it
			        imshow("Tracking video", frame);
			        imshow("Thresholded video", thresholded);
			    }
				break;
			case 4:
				{
					 vector<Point> vp; 
					//Scan for barcodes using zbar, press 'c' for captures.
					std::vector<BarCode> scanned_barcodes = Imgproc::scan_image_zbar(frame);
					// Extract results
			        for (BarCode &bcode : scanned_barcodes) {

			            // print decoded data
			            cout << " decoded " << bcode.m_zbar_type_name << " symbol \"" << bcode.m_zbar_data << '"' << endl;
			         	
			         	for(int i=0;i<bcode.m_zbar_location_size;i++)
			         	{   
				        	vp.push_back(Point(bcode.m_zbar_location_x[i],bcode.m_zbar_location_y[i]));   
				        }

				        RotatedRect r = minAreaRect(vp);   
				        Point2f pts[4];   
				        r.points(pts);   
				        
				        for(int i=0;i<4;i++)
				        {
				        	//draw barcode location box   
				        	line(frame,pts[i],pts[(i+1)%4],Scalar(255,0,0),3);   
				       	}     
			        } 
					namedWindow("Frame", CV_WINDOW_AUTOSIZE);
				    imshow( "Frame", frame );
				    if(key == 'c')
					{
						imwrite(photo_file_name, frame);
						photo_file_name = "corosphoto-";
						photo_file_name.append(std::to_string(file_counter));
						photo_file_name.append(".jpg");
						file_counter++;
					}
				}
				break;
			default:
				break;
		}

	}	

	
}


