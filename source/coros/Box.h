/*
* Copyright Daimler, 2017
* All Rights Reserved
* UNPUBLISHED, LICENSED SOFTWARE.
* CONFIDENTIAL AND PROPRIETARY INFORMATION
* WHICH IS THE PROPERTY OF Daimler.
*
* @file   Box.h
* @date   June, 2017
* @brief  Box Class definitions and related functions
*
* This file defines the functions that the orbbec manager thread uses to create/modify the orbbec devices.
* Each device has a thread and they are all managed by the orbbec manager thread.
*/

#ifndef _BOX_H_
#define _BOX_H_

#include "Common.h"

typedef enum box_s{
	NEW_UNREGISTERED = 0,
	NOT_MOVING,
	MOVING,
	REMOVED,
	UNKNOWN
}box_status;

/**
*	The Box class contains all information and actions needed for a box package
*/
class Box{
	public:
		Box( int box_id); 
		virtual ~Box();

		int         get_box_id();
		double      get_box_volume();
		void 		get_vector_world_pos(float* world_pos); //x,y,z
		float		get_box_length();
		float		get_box_width();
		float		get_box_height();
		box_status  get_box_status();
		std::string get_barcode_data();

		bool set_box_volume(double vol);
		bool set_vector_world_pos(float* world_pos); //x,y,z
		bool set_box_length(float length);
		bool set_box_width(float width);
		bool set_box_height(float height);
		bool set_box_status(box_status status);
		bool set_barcode_data(std::string data);
	
	private:
		int         m_box_id;
		double      m_box_volume;
		float 		m_vector_world_pos[3]; //x,y,z
		float		m_box_length;
		float		m_box_width;
		float		m_box_height;
		box_status  m_curr_box_status;
		std::string m_barcode_data;
};

#endif