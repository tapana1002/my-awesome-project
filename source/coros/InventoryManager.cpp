/*
* Copyright Daimler, 2017
* All Rights Reserved
* UNPUBLISHED, LICENSED SOFTWARE.
* CONFIDENTIAL AND PROPRIETARY INFORMATION
* WHICH IS THE PROPERTY OF Daimler.
*
* @file   InventoryManager.cpp
* @date   June, 2017
* @brief  Inventory Manager related functions
*
*/

#include "InventoryManager.h"

/**
*	InventoryManager constructor
*/
InventoryManager::InventoryManager()
{
	
}

/**
*	destructor of InventoryManager, delete allocated memory resources
*/
InventoryManager::~InventoryManager()
{

}

/**
*	BarCode constructor
*/
BarCode::BarCode()
{
	
}

/**
*	destructor of BarCode, delete allocated memory resources
*/
BarCode::~BarCode()
{

}